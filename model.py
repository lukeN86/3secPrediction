import torch
from torch import nn

from models import resnet, pre_act_resnet, wide_resnet, resnext, densenet, resnet_softatt, resnet_softatt2, resnet_fcn, \
    resnet_stn, resnet_softatt3, resnet_stn3, resnet_fcn2, resnet_fcn3, resnet_lstm, resnet_softatt4, resnet_gap, \
    resnet_softatt5, resnet_softatt6, resnet_gap2, resnet_gap3, resnet_gap4, resnet_velocity, vgg16_attention, velocity


def generate_model(opt):
    assert opt.model in [
        'resnet', 'preresnet', 'wideresnet', 'resnext', 'densenet', 'resnet_softatt','resnet_softatt2', 'resnet_fcn',
        'resnet_stn2', 'resnet_softatt3',  'resnet_stn3', 'resnet_fcn2', 'resnet_fcn3', 'resnet_lstm','resnet_softatt4',
        'resnet_gap',  'resnet_gap2', 'resnet_gap3', 'resnet_gap4', 'resnet_softatt5','resnet_softatt6', 'resnet_velocity',
        'vgg16_attention', 'velocity'
    ]

    if opt.model == 'resnet':
        assert opt.model_depth in [10, 18, 34, 50, 101, 152, 200]

        from models.resnet import get_fine_tuning_parameters

        if opt.model_depth == 10:
            model = resnet.resnet10(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 18:
            model = resnet.resnet18(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 34:
            model = resnet.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 50:
            model = resnet.resnet50(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 101:
            model = resnet.resnet101(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 152:
            model = resnet.resnet152(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 200:
            model = resnet.resnet200(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt':
        assert opt.model_depth in [34]

        from models.resnet_softatt import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt2':
        assert opt.model_depth in [34]

        from models.resnet_softatt2 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt2.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt3':
        assert opt.model_depth in [34]

        from models.resnet_softatt3 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt3.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt4':
        assert opt.model_depth in [34]

        from models.resnet_softatt4 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt4.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt5':
        assert opt.model_depth in [34]

        from models.resnet_softatt5 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt5.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_softatt6':
        assert opt.model_depth in [34]

        from models.resnet_softatt6 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_softatt6.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_fcn':
        assert opt.model_depth in [34]

        from models.resnet_fcn import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_fcn.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_stn2':
        assert opt.model_depth in [34]

        from models.resnet_stn import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_stn.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_stn3':
        assert opt.model_depth in [34]

        from models.resnet_stn3 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_stn3.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_fcn2':
        assert opt.model_depth in [34]

        from models.resnet_fcn2 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_fcn2.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_fcn3':
        assert opt.model_depth in [34]

        from models.resnet_fcn3 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_fcn3.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_lstm':
        assert opt.model_depth in [34]

        from models.resnet_lstm import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_lstm.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_gap':
        assert opt.model_depth in [34]

        from models.resnet_gap import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_gap.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_gap2':
        assert opt.model_depth in [34]

        from models.resnet_gap2 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_gap2.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_gap3':
        assert opt.model_depth in [34]

        from models.resnet_gap2 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_gap3.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_gap4':
        assert opt.model_depth in [34]

        from models.resnet_gap2 import get_fine_tuning_parameters

        if opt.model_depth == 34:
            model = resnet_gap4.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'vgg16_attention':
        assert opt.model_depth in [34]

        if opt.model_depth == 34:
            model = vgg16_attention.vgg16attention(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'resnet_velocity':
        assert opt.model_depth in [34]


        if opt.model_depth == 34:
            model = resnet_velocity.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'velocity':
        assert opt.model_depth in [34]

        if opt.model_depth == 34:
            model = velocity.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'wideresnet':
        assert opt.model_depth in [50]

        from models.wide_resnet import get_fine_tuning_parameters

        if opt.model_depth == 50:
            model = wide_resnet.resnet50(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                k=opt.wide_resnet_k,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
    elif opt.model == 'resnext':
        assert opt.model_depth in [50, 101, 152]

        from models.resnext import get_fine_tuning_parameters

        if opt.model_depth == 50:
            model = resnext.resnet50(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                cardinality=opt.resnext_cardinality,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 101:
            model = resnext.resnet101(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                cardinality=opt.resnext_cardinality,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
        elif opt.model_depth == 152:
            model = resnext.resnet152(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                cardinality=opt.resnext_cardinality,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration,
                num_predictions=opt.num_predictions)
    elif opt.model == 'preresnet':
        assert opt.model_depth in [18, 34, 50, 101, 152, 200]

        from models.pre_act_resnet import get_fine_tuning_parameters

        if opt.model_depth == 18:
            model = pre_act_resnet.resnet18(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 34:
            model = pre_act_resnet.resnet34(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 50:
            model = pre_act_resnet.resnet50(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 101:
            model = pre_act_resnet.resnet101(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 152:
            model = pre_act_resnet.resnet152(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 200:
            model = pre_act_resnet.resnet200(
                num_classes=opt.n_classes,
                shortcut_type=opt.resnet_shortcut,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
    elif opt.model == 'densenet':
        assert opt.model_depth in [121, 169, 201, 264]

        from models.densenet import get_fine_tuning_parameters

        if opt.model_depth == 121:
            model = densenet.densenet121(
                num_classes=opt.n_classes,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 169:
            model = densenet.densenet169(
                num_classes=opt.n_classes,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 201:
            model = densenet.densenet201(
                num_classes=opt.n_classes,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)
        elif opt.model_depth == 264:
            model = densenet.densenet264(
                num_classes=opt.n_classes,
                sample_size=opt.sample_size,
                sample_duration=opt.sample_duration)

    if not opt.no_cuda:
        model = model.cuda()
        model = nn.DataParallel(model, device_ids=None)

    if opt.pretrain_path:
        print('loading pretrained model {}'.format(opt.pretrain_path))
        pretrain = torch.load(opt.pretrain_path)
        assert opt.arch == pretrain['arch']

        state_dict = pretrain['state_dict']

        del state_dict['module.fc.weight']
        del state_dict['module.fc.bias']

        model.load_state_dict(state_dict, strict=False)


    return model, model.parameters()
