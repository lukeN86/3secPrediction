import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import math
from functools import partial
import torchvision


class VGG16Attention(nn.Module):

    def __init__(self,
                 sample_size,
                 sample_duration,
                 shortcut_type='B',
                 num_classes=400,
                 num_predictions=1):
        self.num_classes = num_classes
        self.num_predictions = num_predictions
        self.sample_duration = sample_duration
        self.sample_size = sample_size
        self.hidden_dim = 256

        super(VGG16Attention, self).__init__()

        self.vgg16 = torchvision.models.vgg16(pretrained=True).features

        # self.vgg_action = nn.Sequential(
        #     nn.Conv2d(512, 1024, 3, padding=1),
        #     nn.ReLU(inplace=True),
        #     nn.AvgPool2d(3)
        # )

        # self.avg_pool = nn.AvgPool2d(3)
        #
        # self.vgg_context = nn.Sequential(
        #     nn.Linear(512, 4096),
        #     nn.ReLU(inplace=True),
        #     nn.Dropout(),
        #     nn.Linear(4096, 128),
        #     nn.ReLU(inplace=True),
        #     nn.Dropout()
        # )

        self.conv = nn.Conv2d(512, 128, 3)

        self.lstm = nn.LSTM(128, self.hidden_dim, 1, batch_first=True)

        self.fc = nn.Linear(self.hidden_dim, num_classes * num_predictions)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                m.weight = nn.init.kaiming_normal(m.weight, mode='fan_out')
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, x):

        batch_size = x.size(0)
        x = x.permute([0, 2, 1, 3, 4]).contiguous()
        x = x.view(self.sample_duration * batch_size, 3, self.sample_size, self.sample_size)
        x = self.vgg16(x)

        # x = self.avg_pool(x).squeeze()
        # x = self.vgg_context(x)

        x = self.conv(x)
        x = x.view(batch_size, self.sample_duration, 128)



        # Set initial states
        h0 = Variable(torch.zeros(1, x.size(0), self.hidden_dim).cuda())
        c0 = Variable(torch.zeros(1, x.size(0), self.hidden_dim).cuda())

        # Forward propagate RNN
        out, _ = self.lstm(x, (h0, c0))

        # Decode hidden state of last time step
        out = self.fc(out[:, -1, :])

        if self.num_predictions > 1:
            out = out.view([out.size(0), self.num_classes, self.num_predictions])

        return out




def vgg16attention(**kwargs):
    """Constructs a ResNet-18 model.
    """
    model = VGG16Attention(**kwargs)
    return model


