import math
import cv2
import os
import subprocess
import shlex
import json
import sys
from tqdm import tqdm
from os import listdir
from os.path import isfile, join




def load_from_dir(dataset_directory, subset_name):
    loc = {}
    info_path = join(dataset_directory, 'info', '100k', subset_name)

    for f in tqdm(listdir(info_path)):
        with open(join(info_path, f)) as fp:
            try:
                data = json.load(fp)
                loc[f] = data['locations']
            except:
                print('Cannot process file %s' % f)

    return loc


def find_stoppings(locations):
    MIN_STOP_DURATION = 3
    SPEED_THRESHOLD = 1.0

    prev_speed = float('nan')
    stop_counter = 0
    stop_location = None
    has_stopped = False

    for location in locations:
        speed = location['speed']
        if not math.isnan(prev_speed) and prev_speed > SPEED_THRESHOLD and speed < SPEED_THRESHOLD:
            stop_location = location # Vehicle has stopped
            stop_counter = 0
            has_stopped = True
        elif stop_location is not None and speed < SPEED_THRESHOLD:
            stop_counter += 1
            if stop_counter >= MIN_STOP_DURATION:
                return stop_location, has_stopped
        else:
            stop_location = None
            stop_counter = 0

        prev_speed = speed

    return None, has_stopped


def save_sequence(file_name, locations, stopLocation, secondsBeforeEvent):

    fps = 30
    step = 2

    first_timestamp = locations[0]['timestamp']
    if stopLocation[0] is not None:
        # Vehicle has stopped
        start = stopLocation[0]['timestamp'] - sequenceLength * fps * step - secondsBeforeEvent * 1000
        dirName = 'stoppedIn%02dsec' % secondsBeforeEvent
    elif not stopLocation[1]:
        # Vehicle has never stopped
        start = first_timestamp
        dirName = 'notStopped'
    else:
        # Vehicle has stopped but only for a short period of time -> ignore
        return

    if start < first_timestamp:
        # Not enough data before the event
        return

    name, ext = os.path.splitext(file_name)
    dst_directory_path = os.path.join(targetDir, 'sequences', '100k', subset, dirName, name)
    video_file_path = os.path.join(sourceDir, 'videos', '100k', subset, name + '.mov')

    if not os.path.exists(video_file_path):
        print('file %s not found' % video_file_path)
        return

    if not os.path.exists(dst_directory_path):
        os.makedirs(dst_directory_path)

    startFrame = int((start-first_timestamp) / 1000 * fps)
    extract_video(video_file_path, dst_directory_path, startFrame, sequenceLength, step)





def get_rotation(file_path_with_file_name):
    """
    Function to get the rotation of the input video file.
    Adapted from gist.github.com/oldo/dc7ee7f28851922cca09/revisions using the ffprobe comamand by Lord Neckbeard from
    stackoverflow.com/questions/5287603/how-to-extract-orientation-information-from-videos?noredirect=1&lq=1

    Returns a rotation None, 90, 180 or 270
    """
    cmd = "ffprobe -loglevel error -select_streams v:0 -show_entries stream_tags=rotate -of default=nw=1:nk=1"
    args = shlex.split(cmd)
    args.append(file_path_with_file_name)
    # run the ffprobe process, decode stdout into utf-8 & convert to JSON
    ffprobe_output = subprocess.check_output(args).decode('utf-8')
    if len(ffprobe_output) > 0:  # Output of cmdis None if it should be 0
        ffprobe_output = json.loads(ffprobe_output)
        rotation = ffprobe_output

    else:
        rotation = 0

    return rotation

def extract_video(videoFile, targetDir, startFrame, frameCount, step):

    rotation = get_rotation(videoFile)

    vidcap = cv2.VideoCapture(videoFile)
    success, image = vidcap.read()

    count = 0
    frame = 1
    downscale_factor = 0.25
    success = True
    while success:
        if frame >= startFrame and (frame % step) == 0:
            if rotation == 270:
                image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
            elif rotation == 90:
                image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)

            image = cv2.resize(image, None, fx=downscale_factor, fy=downscale_factor, interpolation=cv2.INTER_CUBIC)

            cv2.imwrite(os.path.join(targetDir, "image_%05d.jpg" % count), image)
            count += 1

        if count >= frameCount:
            break

        success, image = vidcap.read()
        frame += 1

    if count < frameCount:
        print('Failed to process %d frames for the file %s' % (frameCount, videoFile))



# extract_video(r'D:\BDD100k\videos\100k\val\b1d9e136-6c94ea3f.mov', r'C:\Temp\BDD100k', 10, 16, 2)

if len(sys.argv) != 4:
    print('Syntax: extract_sequences_bdd100k.py sourcedir targetdir subset')
    exit(-1)


sourceDir = sys.argv[1]
targetDir = sys.argv[2]
subset = sys.argv[3]

print('Extracting %s from %s to %s' % (subset, sourceDir, targetDir))

sequenceLength = 16





locations = load_from_dir(sourceDir, subset)


for f in tqdm(locations):

    if len(locations[f]) == 0:
        continue

    try:
        stp = find_stoppings(locations[f])
        save_sequence(f, locations[f], stp, 3)
    except:
        print('Cannot process video file %s' % f)





