from __future__ import print_function, division
import os
import sys
import subprocess


def class_process(dir_path, subset):

  videos_dir_path = os.path.join(dir_path, 'videos', '100k', subset)
  dst_dir_path = os.path.join(dir_path, 'jpg', '100k', subset)

  if not os.path.exists(dst_dir_path):
    os.makedirs(dst_dir_path)

  for file_name in os.listdir(videos_dir_path):
    if '.mov' not in file_name:
        continue

    name, ext = os.path.splitext(file_name)
    dst_directory_path = os.path.join(dst_dir_path, name)

    video_file_path = os.path.join(videos_dir_path, file_name)
    try:
      if os.path.exists(dst_directory_path):
        if not os.path.exists(os.path.join(dst_directory_path, 'image_00001.jpg')):
          subprocess.call('rm -r \"{}\"'.format(dst_directory_path), shell=True)
          print('remove {}'.format(dst_directory_path))
          os.mkdir(dst_directory_path)
        else:
          continue
      else:
        os.mkdir(dst_directory_path)
    except:
      print(dst_directory_path)
      continue
    cmd = 'ffmpeg -loglevel panic -hide_banner -i \"{}\" -qscale:v 4 -vf scale=-1:240 -r 5  \"{}/image_%05d.jpg\"'.format(video_file_path, dst_directory_path)
    print(cmd)
    subprocess.call(cmd, shell=True)
    print('\n')


if __name__=="__main__":
  dir_path = sys.argv[1]

  class_process(dir_path, 'train')
  class_process(dir_path, 'val')


