from __future__ import print_function, division
import os
import sys
import subprocess
import csv
from pytube import YouTube
from multiprocessing import Pool
from functools import partial

num_workers = 10

def download_file(video_id, videos_path):
    video_path = os.path.join(videos_path, '%s.mp4' % video_id)
    if os.path.exists(video_path):
        return

    print(video_id)
    yt = YouTube('http://youtube.com/watch?v=' + video_id)
    st = yt.streams.filter(file_extension='mp4', resolution='480p').first()
    if st is None:
        print('Skipping %s' % video_id)
        return ''

    st.download(output_path=videos_path, filename=video_id)

    return videos_path


if __name__=="__main__":
    dir_path = sys.argv[1]

    annotation_file = os.path.join(dir_path, 'bball_dataset_april_4.csv')
    if not os.path.exists(annotation_file):
        print('Annotation file not found')
        exit(-1)

    videos_path = os.path.join(dir_path, 'videos')
    if not os.path.exists(videos_path):
        os.mkdir(videos_path)

    with open(annotation_file, 'r') as csvfile:
        rowreader = csv.reader(csvfile, delimiter=',')
        videos = set()
        for row in rowreader:
            videos.add(row[0])


    videos = list(videos)

    with Pool(num_workers) as p:
        results = p.map(partial(download_file, videos_path=videos_path), videos)

    print(results)



