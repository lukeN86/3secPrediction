
from futureModels.fixedTimeInFuture import FixedTimeInFuture
from futureModels.timeRangeOneInMany import TimeRangeOneInMany
from futureModels.timeRangeToEvent import TimeRangeToEvent
from futureModels.timeInFutureRegression import TimeInFutureRegression
from futureModels.weibull import Weibull
from futureModels.weibulloffset import WeibullOffset
from futureModels.heatmap import Heatmap
from futureModels.gmm import GMM
from futureModels.heatmap2 import Heatmap2

from futureModels.binary_gmm import BinaryGMM
from futureModels.heatmappadded import HeatmapPadded
from futureModels.binarygmmpadded import BinaryGMMPadded
from futureModels.heatmapsym import HeatmapSym
from futureModels.heatmapsyml1 import HeatmapSymL1
from futureModels.heatmapsyml2 import HeatmapSymL2
from futureModels.heatmapsym_extendboundaries import HeatmapSymExtendBoundaries
from futureModels.heatmapsyml2_nooffset import HeatmapSymL2NoOffset
from futureModels.heatmapsymgmm import HeatmapSymGMM
from futureModels.gaussian import Gaussian
from futureModels.heatmapsymzero import HeatmapSymZero

def get_future_prediction_model(opt):


    prediction_suffix = opt.future_prediction

    if opt.future_prediction == 'fixedTime':
        time_in_future_transform = FixedTimeInFuture(opt.seconds_before_event, opt)
        prediction_suffix = "fixedTime_%d" % opt.seconds_before_event
    elif opt.future_prediction == 'timeRangeOneInMany':
        time_in_future_transform = TimeRangeOneInMany(opt)
    elif opt.future_prediction == 'timeRangeToEvent':
        time_in_future_transform = TimeRangeToEvent(opt)
    elif opt.future_prediction == 'timeInFutureRegression':
        time_in_future_transform = TimeInFutureRegression(opt)
    elif opt.future_prediction == 'weibull':
        time_in_future_transform = Weibull(opt)
    elif opt.future_prediction == 'weibulloffset':
        time_in_future_transform = WeibullOffset(opt)
    elif opt.future_prediction == 'heatmap':
        time_in_future_transform = Heatmap(opt)
    elif opt.future_prediction == 'heatmap2':
        time_in_future_transform = Heatmap2(opt)
    elif opt.future_prediction == 'gmm11':
        time_in_future_transform = GMM(opt)
    elif opt.future_prediction == 'binarygmm':
        time_in_future_transform = BinaryGMM(opt)
    elif opt.future_prediction == 'heatmappadded':
        time_in_future_transform = HeatmapPadded(opt)
        prediction_suffix = "%s_hps%d" % (opt.future_prediction, opt.heatmap_resolution_per_second)
    elif opt.future_prediction == 'binarygmmpadded5':
        time_in_future_transform = BinaryGMMPadded(opt)
    elif opt.future_prediction == 'heatmapsym':
        time_in_future_transform = HeatmapSym(opt)
        prediction_suffix = "%s_hps%d" % (opt.future_prediction, opt.heatmap_resolution_per_second)
    elif opt.future_prediction == 'heatmapsyml2':
        time_in_future_transform = HeatmapSymL2(opt)
    elif opt.future_prediction == 'heatmapsyml1':
        time_in_future_transform = HeatmapSymL1(opt)
    elif opt.future_prediction ==  'heatmapsymextendboundaries':
        time_in_future_transform = HeatmapSymExtendBoundaries(opt)
    elif opt.future_prediction ==  'heatmapsyml2_nooffset':
        time_in_future_transform = HeatmapSymL2NoOffset(opt)
    elif opt.future_prediction == 'heatmapsymgmm4':
        time_in_future_transform = HeatmapSymGMM(opt)
    elif opt.future_prediction == 'gaussian':
        time_in_future_transform = Gaussian(opt)
    elif opt.future_prediction == 'heatmapsymzero':
        time_in_future_transform = HeatmapSymZero(opt)
        prediction_suffix = "%s_hps%d" % (opt.future_prediction, opt.heatmap_resolution_per_second)


    else:
        assert False

    opt.num_predictions = time_in_future_transform.get_num_predictions()
    opt.n_classes = time_in_future_transform.get_num_classes()

    return opt, time_in_future_transform, prediction_suffix