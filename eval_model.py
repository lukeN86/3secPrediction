import time
import torch
from torch.autograd import Variable
from opts import parse_opts

from utils import AverageMeter, calculate_accuracy, calculate_gt_likelihood
from model_loader import load_model
from dataset import get_validation_data

import numpy as np
import os



def val_epoch(data_loader, model, opt, time_in_future_transform, video_accuracy):

    model.eval()

    event_accuracies = AverageMeter()
    tte_accuracies = AverageMeter()
    gt_likelihoods = AverageMeter()

    tte_dist = np.zeros(opt.max_prediction_horizon * 2, dtype=int)
    pred_dist = np.zeros(opt.max_prediction_horizon * 2, dtype=int)

    video_ix = 0
    for i, (inputs, targets, velocities) in enumerate(data_loader):

        with torch.no_grad():
            targets = time_in_future_transform.reshape_targets(targets)

            if targets.dtype == torch.float64:
                targets = targets.float()

            if not opt.no_cuda:
                targets = targets.cuda(async=True)

            inputs = Variable(inputs)
            targets = Variable(targets)

            if hasattr(model.module, 'has_velocity'):
                if velocities.dtype == torch.float64:
                    velocities = velocities.float()

                velocities = Variable(velocities)
                outputs = model(inputs, velocities)
            else:
                outputs = model(inputs)

            gt_tte, pred_tte = calculate_accuracy(outputs, targets, time_in_future_transform, opt, event_accuracies, tte_accuracies)

            calculate_gt_likelihood(outputs, targets, time_in_future_transform, opt, gt_likelihoods)

            # calculate_cdf_error(outputs, targets, time_in_future_transform, opt, cdf_error)


            gt_tte = gt_tte.cpu().numpy()
            pred_tte = pred_tte.cpu().numpy()

            for tte in gt_tte:
                ix = int(tte) - 1
                tte_dist[ix] = tte_dist[ix] + 1

            for tte in pred_tte:
                ix = int(tte) - 1
                pred_dist[ix] = pred_dist[ix] + 1

            for j in range(len(gt_tte)):
                eq = abs(gt_tte[j] - pred_tte[j]) < 2.
                if eq:
                    video_accuracy[video_ix,0] = video_accuracy[video_ix,0] + 1

                video_accuracy[video_ix,1] = int(gt_tte[j] <= 10)
                video_ix = video_ix + 1



    print(tte_dist)
    print(pred_dist)

    return event_accuracies, tte_accuracies, gt_likelihoods


if __name__ == '__main__':
    opt = parse_opts()

    model, time_in_future_transform = load_model(opt)

    validation_data = get_validation_data(opt, time_in_future_transform)

    val_loader = torch.utils.data.DataLoader(
        validation_data,
        batch_size=opt.batch_size,
        shuffle=False,
        num_workers=opt.n_threads,
        pin_memory=True)

    accuracy = np.zeros((opt.max_prediction_horizon, 3))

    video_accuracy = np.zeros((len(validation_data), 2))

    for i in range(1, opt.max_prediction_horizon+1):

        time_in_future_transform.force_seconds_before_event(i)

        event_acc, tte_acc, gt_likelihoods = val_epoch(val_loader, model, opt, time_in_future_transform, video_accuracy)
        accuracy[i-1, 0] = event_acc.avg
        accuracy[i-1, 1] = tte_acc.avg
        accuracy[i-1, 2] = gt_likelihoods.avg
        print('{i}s: \t event accuracy {event_acc.avg:.3f} \t TTE error {tte_acc.avg:.3f} \t GT LL {gt_likelihoods.avg:.3f} '.format(i=i, event_acc=event_acc, tte_acc=tte_acc,gt_likelihoods=gt_likelihoods))



    parent_dir, dirname = os.path.split(opt.val_model_path)
    np.save(os.path.join(parent_dir, '%s_%s_eval.npy' % (dirname, opt.dataset)), accuracy)

    print(accuracy[:,0].mean()*100.)
    print(accuracy[:,1].mean())
    print(accuracy[:,2].mean())

    # for i in range(video_accuracy.shape[0]):
    #     print('video %d = %d (%d)' % (i, video_accuracy[i,0], video_accuracy[i,1]))






