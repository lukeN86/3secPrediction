import os
import sys
import json

import numpy as np

max_prediction_horizon = 10

if __name__=="__main__":
    dir_path = sys.argv[1]

    accuracy = np.zeros((10, 1), dtype=float)
    cnt = np.zeros((10, 1), dtype=float)

    responses_per_ip = dict()
    correct_responses_per_ip = dict()

    valid_ips = set()

    for f in os.listdir(dir_path):
        with open(os.path.join(dir_path, f)) as fp:
            data = json.load(fp)

        ip = data['ip']
        if ip not in responses_per_ip:
            responses_per_ip[ip] = 0
            correct_responses_per_ip[ip] = 0

        responses_per_ip[ip] = responses_per_ip[ip] + 1
        if data['label'] == data['response']:
            correct_responses_per_ip[ip] = correct_responses_per_ip[ip] + 1

    for ip in responses_per_ip:
        ratio = float(correct_responses_per_ip[ip]) / responses_per_ip[ip]
        if ratio > 0.55:
            valid_ips.add(ip)


    print(len(valid_ips))



    valid_cnt = 0
    for f in os.listdir(dir_path):
        with open(os.path.join(dir_path, f)) as fp:
            data = json.load(fp)

        if data['ip'] not in valid_ips:
            continue

        valid_cnt = valid_cnt + 1

        if data['label'] == 0:
            if data['response'] == 0:
                accuracy[:, 0] = accuracy[:, 0] + 1

            cnt[:, 0] = cnt[:, 0] + 1
        else:
            ix = data['beforeEvent'] - 1
            if data['response'] == 1:
                accuracy[ix, 0] = accuracy[ix, 0]

            cnt[ix, 0] = cnt[ix, 0] + 1



    print(valid_cnt)
    print(accuracy / cnt)

    parent_dir, dirname = os.path.split(dir_path)
    np.save(os.path.join(parent_dir, 'human_human_eval.npy'), accuracy / cnt)
