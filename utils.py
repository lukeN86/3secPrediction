import csv
import torch
import os
import numpy as np
from PIL import Image
import functools
import math


from spatial_transforms import ToTensor

class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class Logger(object):

    def __init__(self, path, header):
        self.log_file = open(path, 'w')
        self.logger = csv.writer(self.log_file, delimiter='\t')

        self.logger.writerow(header)
        self.header = header

    def __del(self):
        self.log_file.close()

    def log(self, values):
        write_values = []
        for col in self.header:
            assert col in values
            write_values.append(values[col])

        self.logger.writerow(write_values)
        self.log_file.flush()


def load_value_file(file_path):
    with open(file_path, 'r') as input_file:
        value = float(input_file.read().rstrip('\n\r'))

    return value


def calculate_accuracy(outputs, targets, time_in_future_transform, opt, event_accuracies, tte_accuracies):
    batch_size = targets.size(0)

    gt_stopped, gt_tte, _ = time_in_future_transform.map_representation(targets)
    pred_stopped, pred_tte, _ = time_in_future_transform.map_representation(outputs)

    #Event accuracy
    correct = pred_stopped.eq(gt_stopped).float()
    n_correct_elems = correct.sum().item()
    event_accuracies.update(n_correct_elems / batch_size, batch_size)

    #TTE accuracy
    tte_acc = torch.abs(pred_tte - gt_tte) * correct + (1-correct) * opt.max_prediction_horizon
    gt_stopped_float = gt_stopped.float()
    tte_acc = tte_acc * gt_stopped_float
    num_stopped = torch.sum(gt_stopped_float)
    if num_stopped > 0:
        tte_accuracies.update(torch.sum(tte_acc) / num_stopped, num_stopped)

    # if targets.dtype == torch.float32:
    #     #  This is regression, return MSE
    #     pred = time_in_future_transform.map_representation(outputs)
    #     targets = time_in_future_transform.map_representation(targets)
    #     return
    # else:
    #
    #     _, pred = outputs.topk(1, 1, True)
    #
    #     pred = time_in_future_transform.map_representation(pred)
    #     targets = time_in_future_transform.map_representation(targets)
    #
    #     pred = pred.t()

    return gt_tte, pred_tte




def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def gaussian_torch(x, mu, sig):
    return torch.exp(-torch.pow(x - mu, 2.) / (2 * np.power(sig, 2.)))

def gaussian_cdf(x, mu, sig):
    return 0.5 * (1 + torch.erf((x - mu) * sig / math.sqrt(2)))


def calculate_gt_likelihood(outputs, targets, time_in_future_transform, opt, gt_likelihood):
    if not hasattr(time_in_future_transform, 'create_pdf'):
        return

    gt_stopped, gt_tte, _ = time_in_future_transform.map_representation(targets)
    gt_tte = gt_tte.cpu()
    pred_stopped, pred_tte, _ = time_in_future_transform.map_representation(outputs)


    pdf = time_in_future_transform.create_pdf(outputs)

    for i in range(outputs.size(0)):
        if not gt_stopped[i]:
            continue

        ix = int((gt_tte[i] - 1) * opt.eval_resolution_per_second)
        prob = -math.log(pdf[i, ix])
        gt_likelihood.update(prob, 1)


def calculate_cdf_error(outputs, targets, time_in_future_transform, opt, cdf_error):
    gt_stopped, gt_tte, _ = time_in_future_transform.map_representation(targets)
    gt_tte = gt_tte.cpu()
    pred_stopped, pred_tte, _ = time_in_future_transform.map_representation(outputs)

    if hasattr(time_in_future_transform, 'create_cdf'):
        cdf = time_in_future_transform.create_cdf(outputs)
    else:
        cdf = create_cdf(pred_tte, opt)


    for i in range(outputs.size(0)):
        if not gt_stopped[i]:
            continue


        predictions_count = opt.eval_resolution_per_second * opt.max_prediction_horizon
        pred_cdf = cdf[i, :predictions_count].cpu().numpy()

        target_cdf = torch.arange(1, opt.max_prediction_horizon + 1, 1. / opt.eval_resolution_per_second, dtype=torch.float32)
        target_cdf = gaussian_cdf(target_cdf, gt_tte[i], 1.).numpy()

        diff = np.abs(target_cdf - pred_cdf)

        error = (np.sum(diff) / predictions_count)
        cdf_error.update(error, 1)




def create_cdf(tte, opt):


    predictions_count = opt.eval_resolution_per_second * opt.max_prediction_horizon
    cdf = torch.zeros(tte.size(0), predictions_count, dtype=torch.float32)

    for i in range(tte.size(0)):
        if tte[i] < opt.max_prediction_horizon:
            ix = int((tte[i] * opt.eval_resolution_per_second))
            cdf[i, ix:] = 1.

    return cdf


def first_nonzero(x, axis=0):
    nonz = (x > 0)
    return ((nonz.cumsum(axis) == 1) & nonz).max(axis)



def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


def accimage_loader(path):
    try:
        import accimage
        return accimage.Image(path)
    except IOError:
        # Potentially a decoding problem, fall back to PIL.Image
        return pil_loader(path)


def get_default_image_loader():
    from torchvision import get_image_backend
    if get_image_backend() == 'accimage':
        return accimage_loader
    else:
        return pil_loader



def video_loader(video_dir_path, frame_indices, image_loader):
    video = []
    for i in frame_indices:
        image_path = os.path.join(video_dir_path, 'image_{:05d}.jpg'.format(i))
        if os.path.exists(image_path):
            video.append(image_loader(image_path))
        else:
            print('Image %s not found' % image_path)
            return video

    return video


def get_default_video_loader():
    image_loader = get_default_image_loader()
    return functools.partial(video_loader, image_loader=image_loader)

def print_dataset_mean(dataset):

    loader = get_default_video_loader()

    cumMean = torch.zeros(1, 3)
    cnt = 0
    tranform = ToTensor(norm_value=1)
    for sample in dataset:
        if sample['label'] == 1:
            frame = loader(sample['video'], [sample['stop_frame_index']])
            frame = tranform(frame[0]).float()

            mean = frame.sum(dim=1).sum(dim=1) / frame.size(1) / frame.size(2)
            cumMean = cumMean + mean
            cnt = cnt + 1

    cumMean = cumMean / cnt

    print(cnt)
    print(cumMean)


class FrameIterator(object):

    def __init__(self, opt):
        self.start = 1
        self.opt = opt

    def get_frame_indices_and_label(self, sample):
        n_frames = self.opt.sample_duration

        assert self.start >= 1
        assert (self.start + n_frames) <= sample['n_frames']

        return list(range(self.start, self.start + n_frames)), sample

    def set_frame(self, start):
        assert start >= 1
        self.start = start