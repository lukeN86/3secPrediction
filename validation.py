import torch
from torch.autograd import Variable
import time
import sys

from utils import AverageMeter, calculate_accuracy


def val_epoch(epoch, data_loader, model, criterion, opt, logger, time_in_future_transform):
    print('validation at epoch {}'.format(epoch))

    model.eval()

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    event_accuracies = AverageMeter()
    tte_accuracies = AverageMeter()

    end_time = time.time()
    for i, (inputs, targets, velocities) in enumerate(data_loader):
        data_time.update(time.time() - end_time)

        with torch.no_grad():
            targets = time_in_future_transform.reshape_targets(targets)

            if targets.dtype == torch.float64:
                targets = targets.float()

            if not opt.no_cuda:
                targets = targets.cuda(async=True)

            inputs = Variable(inputs)
            targets = Variable(targets)

            if hasattr(model.module, 'has_velocity'):
                if velocities.dtype == torch.float64:
                    velocities = velocities.float()

                velocities = Variable(velocities)
                outputs = model(inputs, velocities)
            else:
                outputs = model(inputs)

            loss = criterion(outputs, targets)
            calculate_accuracy(outputs, targets, time_in_future_transform, opt, event_accuracies, tte_accuracies)

        losses.update(loss.data[0], inputs.size(0))

        batch_time.update(time.time() - end_time)
        end_time = time.time()

        print('Epoch: [{0}][{1}/{2}]\t'
              'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
              'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
              'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
              'Event Acc {event_acc.val:.3f} ({event_acc.avg:.3f})\t'
              'TTE Acc {tte_acc.val:.3f} ({tte_acc.avg:.3f})\t'.format(
                  epoch,
                  i + 1,
                  len(data_loader),
                  batch_time=batch_time,
                  data_time=data_time,
                  loss=losses,
                  event_acc=event_accuracies,
                  tte_acc=tte_accuracies))

    logger.log({'epoch': epoch, 'loss': losses.avg, 'event_acc': event_accuracies.avg, 'tte_acc': tte_accuracies.avg})

    return losses.avg
