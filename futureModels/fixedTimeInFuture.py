import random
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable


class FixedTimeInFuture(object):

    def __init__(self, seconds_before_event, opt):
        self.seconds_before_event = seconds_before_event
        self.opt = opt

    def get_training_criterion(self):
        return nn.CrossEntropyLoss()

    def get_frame_indices_and_label(self, sample):
        n_frames = 16

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - self.seconds_before_event * self.opt.images_per_second - n_frames
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), sample['label']

    def map_representation(self, outputs):
        return outputs

    def get_num_predictions(self):
        return 1

    def get_num_classes(self):
        return 2

    def get_sample_weights(self, target):
        weight = [1, 1]
        samples_weight = np.array([weight[t] for t in target])
        return samples_weight

    def force_seconds_before_event(self, seconds_before_event):
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))

    def create_predictor(self, model, data_loader, dataset):
        pred = FixedTimeInFuturePredictor(self, self.opt)
        pred.evaluate_model(model, data_loader, dataset)
        return pred


class FixedTimeInFuturePredictor(object):
    def __init__(self, time_in_future_transform, opt):
        self.predictions = None
        self.targets = None
        self.time_in_future_transform = time_in_future_transform
        self.opt = opt

    def load_model_state_for_validation(self, model, seconds_before_event):
        model_path = os.path.join(self.opt.val_model_path, "pred%d.pth" % seconds_before_event)
        print('loading model {}'.format(model_path))
        checkpoint = torch.load(model_path)
        assert self.opt.arch == checkpoint['arch']
        model.load_state_dict(checkpoint['state_dict'])

    def evaluate_model(self, model, data_loader, dataset):
        self.predictions = np.zeros((len(dataset), self.opt.max_prediction_horizon, self.opt.max_prediction_horizon))
        self.targets = np.zeros(len(dataset), dtype=int)

        for i in range(self.opt.max_prediction_horizon):
            self.load_model_state_for_validation(model, i+1)
            model.eval()
            for j in range(self.opt.max_prediction_horizon):
                self.time_in_future_transform.force_seconds_before_event(j+1)
                ix = 0
                for _, (inputs, targets) in enumerate(data_loader):
                    with torch.no_grad():
                        inputs = Variable(inputs)
                        outputs = model(inputs)

                    _, pred = outputs.topk(1, 1, True)
                    for k in range(outputs.size(0)):
                        self.predictions[ix, j, i] = pred[k]
                        if i == 0 and j == 0:
                            self.targets[ix] = targets[k]

                        ix = ix + 1

    def get_prediction(self, sample, seconds_before_event):
        return self.predictions[sample, seconds_before_event-1, :]

    def get_target(self, sample):
        return self.targets[sample]
