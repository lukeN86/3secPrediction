import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable



class TimeRangeOneInMany(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return nn.CrossEntropyLoss()

    def get_frame_indices_and_label(self, sample):
        n_frames = self.opt.sample_duration

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label = seconds_before_event
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            label = 0

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), label

    def map_representation(self, outputs):

        if outputs.dtype == torch.float32:
            _, outputs = outputs.topk(1, 1, True)

        outputs = torch.squeeze(outputs)
        has_stopped = outputs > 0
        tte = has_stopped.float() * outputs.float()

        return has_stopped, tte, outputs

    def get_num_predictions(self):
        return 1

    def get_num_classes(self):
        return self.opt.max_prediction_horizon + 1

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))

    def get_sample_weights(self, target):
        weight = [1. / self.opt.max_prediction_horizon, 1.]
        samples_weight = np.array([weight[t] for t in target])
        return samples_weight







