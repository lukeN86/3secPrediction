import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable
from utils import first_nonzero


class TimeRangeToEvent(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return nn.CrossEntropyLoss()

    def get_frame_indices_and_label(self, sample):
        n_frames = self.opt.sample_duration

        label = torch.zeros(self.opt.max_prediction_horizon + 1, dtype=torch.int64)

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label[(seconds_before_event-1):] = 1
        else:
            # Not stopped
            if 'max_frame_count' in sample:
                frame_ix = sample['event_start_time']
                start = random.randint(frame_ix, frame_ix + sample['max_frame_count'] - n_frames)
            else:
                rand_end = sample['n_frames'] - n_frames
                start = random.randint(1, rand_end)
            label[-1] = 1

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), label

    def map_representation(self, outputs):

        if outputs.dim() == 3:
            _, outputs = outputs.topk(1, 1, True)
            outputs = outputs.view(outputs.size(0), -1)

        _, tte = first_nonzero(outputs, 1)
        has_stopped = tte < self.opt.max_prediction_horizon

        has_stopped_float = has_stopped.float()
        tte = (tte.float() + 1.) * has_stopped_float + 2 * self.opt.max_prediction_horizon * (1. - has_stopped_float)

        return has_stopped, tte, outputs

    def get_num_predictions(self):
        return self.opt.max_prediction_horizon + 1

    def get_num_classes(self):
        return 2

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))

    def get_sample_weights(self, target):
        weight = [1, 1]
        samples_weight = np.array([weight[t] for t in target])
        return samples_weight

