import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable
from utils import gaussian


sigma = 2.
no_event_margin_in_seconds = 2


class HeatmapSym(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return nn.MSELoss()

    def get_frame_indices_and_label(self, sample):
        n_frames = self.opt.sample_duration



        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            seconds_before_event = random.randint(self.opt.max_prediction_horizon + no_event_margin_in_seconds, self.opt.max_prediction_horizon * 2 + 1)

        label = np.arange(1, self.opt.max_prediction_horizon * 2 + 1, 1. / self.opt.heatmap_resolution_per_second, dtype=float)
        label = gaussian(label, seconds_before_event, sigma)

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), label

    def map_representation(self, outputs):
        ix = torch.argmax(outputs, 2).float()
        tte = (ix / self.opt.heatmap_resolution_per_second) + 1.
        stopping_threshold = self.opt.max_prediction_horizon + (no_event_margin_in_seconds / 2)
        has_stopped = tte <= stopping_threshold
        has_stopped_float = has_stopped.float()

        dist = (has_stopped_float * tte) + (1 - has_stopped_float) * self.opt.max_prediction_horizon * 2

        output_sum = outputs.sum(dim=2)
        output_sum = output_sum.repeat(1, outputs.size(2)).view(outputs.size(0), 1, outputs.size(2))
        output_norm = outputs / output_sum

        return has_stopped, dist, output_norm


    def get_num_predictions(self):
        return self.opt.max_prediction_horizon * 2 * self.opt.heatmap_resolution_per_second

    def get_num_classes(self):
        return 1

    def reshape_targets(self, targets):
        return targets.view(targets.size(0), 1, self.opt.max_prediction_horizon * 2 * self.opt.heatmap_resolution_per_second)

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))

    def create_cdf(self, outputs):
        _, _, output_norm = self.map_representation(outputs)
        cdf = output_norm.cumsum(dim=2).squeeze()
        return cdf

    def create_pdf(self, outputs):
        _, _, output_norm = self.map_representation(outputs)

        prob = output_norm.squeeze()
        prob = (prob > 0).float() * prob + 1e-6
        return prob





