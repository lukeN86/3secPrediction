import random
import torch
import math
import numpy as np
import os
import torch

from torch import nn
from torch.autograd import Variable



class WeibullOffset(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return WeinbullLoss(self.opt.max_prediction_horizon * 2)

    def get_frame_indices_and_label(self, sample):
        n_frames = 16

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label = seconds_before_event
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            label = self.opt.max_prediction_horizon * 2

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), float(label)

    def map_representation(self, outputs):
        assert outputs.size(2) == 3
        s = nn.functional.softplus(outputs[:, :, 2])

        not_stopped = (s > self.opt.max_prediction_horizon).float()

        a = (1-not_stopped)*s + not_stopped * (self.opt.max_prediction_horizon * 2)

        return a

    def get_num_predictions(self):
        return 3

    def get_num_classes(self):
        return 1

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))





class WeinbullLoss(nn.Module):
    def __init__(self, censored_label):
        super(WeinbullLoss, self).__init__()
        self.censored_label = censored_label



    def forward(self, input, target):
        assert input.size(2) == 3
        a = nn.functional.softplus(input[:, :, 0])
        b = nn.functional.softplus(input[:, :, 1])
        s = nn.functional.softplus(input[:, :, 2])
        y = target
        u = (target < self.censored_label).float()

        likelihood = -self.weibull_loglikelihood_continuous(a, b, s, y, u) + self.weibull_beta_penalty(b)

        return torch.mean(likelihood)

    def weibull_loglikelihood_continuous(self, a_, b_, s_, y_, u_):
        ya = torch.div(y_ - s_ + 1e-35, a_)
        return (
                torch.mul(u_,
                          torch.log(b_) + torch.mul(b_, torch.log(ya))
                       ) -
                torch.pow(ya, b_)
        )

    def weibull_beta_penalty(self, b_, location=10.0, growth=20.0):
        # Regularization term to keep beta below location

        scale = growth / location
        penalty_ = torch.exp(scale * (b_ - location))

        return penalty_
