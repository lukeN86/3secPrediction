import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable
from utils import gaussian

heatmap_resolution_per_second = 5
sigma = 2.
stopping_threshold = .3


class Heatmap(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return nn.MSELoss()

    def get_frame_indices_and_label(self, sample):
        n_frames = 16



        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second

            label = np.arange(1, self.opt.max_prediction_horizon + 1, 1. / heatmap_resolution_per_second, dtype=float)
            label = gaussian(label, seconds_before_event, sigma)

        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)

            label = np.zeros(self.opt.max_prediction_horizon * heatmap_resolution_per_second, dtype=float)

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), label

    def map_representation(self, outputs):
        has_stopped = (torch.sum(outputs > stopping_threshold, dim=2) > 0).float()
        ix = torch.argmax(outputs, 2).float()
        tte = (ix / heatmap_resolution_per_second) + 1.

        dist = (has_stopped * tte) + (1 - has_stopped) * self.opt.max_prediction_horizon * 2

        return has_stopped, dist, outputs


    def get_num_predictions(self):
        return self.opt.max_prediction_horizon * heatmap_resolution_per_second

    def get_num_classes(self):
        return 1

    def reshape_targets(self, targets):
        return targets.view(targets.size(0), 1, self.opt.max_prediction_horizon * heatmap_resolution_per_second)

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))





