import random
import torch
import math
import numpy as np
import os
import torch

from torch import nn
from torch.autograd import Variable
from utils import first_nonzero

stopping_threshold = 0.8

class Weibull(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return WeinbullLoss(self.opt.max_prediction_horizon * 2)

    def get_frame_indices_and_label(self, sample):
        n_frames = 16

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label = seconds_before_event
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            label = self.opt.max_prediction_horizon * 2

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), float(label)

    def map_representation(self, outputs):

        if outputs.dim() > 1:
            a = nn.functional.softplus(outputs[:, :, 0])
            b = nn.functional.softplus(outputs[:, :, 1])

            pred = torch.arange(1, self.opt.max_prediction_horizon + 1, 1. / self.opt.eval_resolution_per_second, dtype=torch.float32).cuda()

            a = a.repeat(1, pred.size(0))
            b = b.repeat(1, pred.size(0))
            pred = pred.repeat(outputs.size(0), 1)

            # 1- e^{-(t/\alpha)^\beta}
            pred = 1 - torch.exp(-torch.pow((pred / a), b))

            stopped = pred > stopping_threshold
            _, ix = first_nonzero(stopped, 1)

            has_stopped = torch.sum(stopped, 1) > 0.

            tte = (ix.float() / self.opt.eval_resolution_per_second) + 1.

        else:
            has_stopped = outputs <= self.opt.max_prediction_horizon
            tte = outputs

        has_stopped_float = has_stopped.float()
        tte = tte * has_stopped_float + 2 * self.opt.max_prediction_horizon * (1. - has_stopped_float)
        return has_stopped, tte, outputs




    def get_num_predictions(self):
        return 2

    def get_num_classes(self):
        return 1

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))

    def create_pdf(self, outputs):
        a = nn.functional.softplus(outputs[:, :, 0])
        b = nn.functional.softplus(outputs[:, :, 1])

        pred = torch.arange(1, self.opt.max_prediction_horizon + 1, 1. / self.opt.eval_resolution_per_second,
                            dtype=torch.float32).cuda()

        a = a.repeat(1, pred.size(0))
        b = b.repeat(1, pred.size(0))
        pred = pred.repeat(outputs.size(0), 1)

        # 1- e^{-(t/\alpha)^\beta}
        pred = 1 - torch.exp(-torch.pow((pred / a), b))

        pdf = pred + 1e-9

        output_sum = pdf.sum(dim=1)
        output_sum = output_sum.repeat(1, pdf.size(1)).view(pdf.size(0), pdf.size(1))
        output_norm = pdf / output_sum

        return output_norm




class WeinbullLoss(nn.Module):
    def __init__(self, censored_label):
        super(WeinbullLoss, self).__init__()
        self.censored_label = censored_label



    def forward(self, input, target):
        assert input.size(2) == 2
        a = nn.functional.softplus(input[:, :, 0])
        b = nn.functional.softplus(input[:, :, 1])
        y = target
        u = (target < self.censored_label).float()

        likelihood = -self.weibull_loglikelihood_continuous(a, b, y, u) + self.weibull_beta_penalty(b)

        return torch.mean(likelihood)

    def weibull_loglikelihood_continuous(self, a_, b_, y_, u_):
        ya = torch.div(y_ + 1e-35, a_)
        return (
                torch.mul(u_,
                          torch.log(b_) + torch.mul(b_, torch.log(ya))
                       ) -
                torch.pow(ya, b_)
        )

    def weibull_beta_penalty(self, b_, location=10.0, growth=20.0):
        # Regularization term to keep beta below location

        scale = growth / location
        penalty_ = torch.exp(scale * (b_ - location))

        return penalty_
