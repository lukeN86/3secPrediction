import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable

from torch import functional as F

heatmap_resolution_per_second = 4
num_predictors = 10




def gaussian(x, mu, sig):
    return torch.exp(-torch.pow(x - mu, 2.) / (2 * sig))
    #* (1. / (torch.sqrt(2 * math.pi * torch.pow(sig, 2.))))


class BinaryGMM(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return GMMLoss(self.opt.max_prediction_horizon)

    def get_frame_indices_and_label(self, sample):
        n_frames = 16



        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label = seconds_before_event

        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)

            label = self.opt.max_prediction_horizon * 2

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), label

    def map_representation(self, outputs):
        if outputs.dim() > 1:
            mu = outputs[:, 0, :]
            sigma = nn.functional.softplus(outputs[:, 1, :])
            w = nn.functional.softmax(outputs[:, 2:, :], 1)
            _, w = w.topk(1, 1, True)
            w = torch.squeeze(w).float()

            has_stopped = torch.sum(w, 1) > 0.

            pred_centers = torch.arange(1, self.opt.max_prediction_horizon + 1, self.opt.max_prediction_horizon  / num_predictors, dtype=torch.float32).cuda()
            pred_centers = pred_centers.repeat(outputs.size(0), 1)
            mu = mu + pred_centers



            pred = torch.arange(1, self.opt.max_prediction_horizon + 1, 1. / self.opt.eval_resolution_per_second,
                                dtype=torch.float32).cuda()

            mu = mu.repeat(pred.size(0), 1, 1).permute([1, 2, 0])
            sigma = sigma.repeat(pred.size(0), 1, 1).permute([1, 2, 0])
            w = w.repeat(pred.size(0), 1, 1).permute([1, 2, 0])
            pred = pred.repeat(outputs.size(0), num_predictors, 1)


            pred = gaussian(pred, mu, sigma) * w
            pred = pred.sum(dim=1).squeeze()

            ix = torch.argmax(pred, 1).float()

            #has_stopped = torch.sum(stopped, 1) > 0.

            tte = (ix.float() / self.opt.eval_resolution_per_second) + 1.


        else:
            has_stopped = outputs <= self.opt.max_prediction_horizon
            tte = outputs.float()

        has_stopped_float = has_stopped.float()
        tte = tte * has_stopped_float + 2 * self.opt.max_prediction_horizon * (1. - has_stopped_float)
        return has_stopped, tte, outputs


    def get_num_predictions(self):
        return num_predictors

    def get_num_classes(self):
        return 4

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))


    def init_model(self, model):
        for m in model.modules():
            if isinstance(m, nn.Linear):
                m.weight[:, num_predictors:num_predictors*2].data.zero_()
                m.bias[num_predictors:num_predictors*2].data.copy_(torch.ones(num_predictors, dtype=torch.float) * 2.)
                return



class GMMLoss(nn.Module):
    def __init__(self, max_prediction_horizon):
        super(GMMLoss, self).__init__()
        self.max_prediction_horizon = max_prediction_horizon


    def forward(self, input, target):
        assert input.size(2) == num_predictors
        assert input.size(1) == 4

        batch_size = input.size(0)

        mu = input[:, 0, :]
        sigma = nn.functional.softplus(input[:, 1, :])


        #Classification
        binary_target = torch.zeros(batch_size, num_predictors, dtype=torch.int64).cuda()

        for i in range(batch_size):
            if target[i] <= self.max_prediction_horizon:
                binary_target[i, (target[i] - 1)] = 1

        cls = input[:, 2:, :]
        logit = nn.functional.log_softmax(cls, 1)
        binary_loss = nn.functional.nll_loss(logit, binary_target)

        #Regression
        pred_centers = torch.arange(1, self.max_prediction_horizon + 1, self.max_prediction_horizon / num_predictors, dtype=torch.float32).cuda()
        pred_centers = pred_centers.repeat(batch_size, 1)
        regression_target = target.repeat(num_predictors, 1).t().float()

        offset = regression_target - pred_centers
        likelihood = torch.sum(gaussian(mu, offset, sigma) * binary_target.float(), 1) + 1e-6
        has_stopped = (target <= self.max_prediction_horizon).float()
        regression_loss = -torch.log(likelihood) * has_stopped
        regression_loss = torch.sum(regression_loss) / torch.sum(has_stopped)

        return 10. * binary_loss + regression_loss


        # max_mu = self.max_prediction_horizon + not_stopped_horizon
        #
        # if torch.sum(has_stopped).item() > 0:
        #     likelihood1 = torch.sum(gaussian(mu, offset, sigma), 1) + 1e-6
        #     loss1 = -torch.log(likelihood1) * has_stopped
        #     loss1 = torch.sum(loss1) / torch.sum(has_stopped)
        #     print('L1: %.3f' % loss1.item())
        # else:
        #     loss1 = 0
        #
        # if torch.sum(1 - has_stopped).item() > 0:
        #     sigma_tensor = torch.ones_like(mu) * 5.
        #
        #     likelihood2 = torch.sum(gaussian(mu, max_mu, sigma_tensor), 1) + 1e-6
        #     loss2 = -torch.log(likelihood2) * (1 - has_stopped)
        #     loss2 = torch.sum(loss2) / torch.sum(1 - has_stopped) * 0.01
        #     # loss2 = torch.mean(torch.pow(mu - max_mu, 2.)/max_mu**2, 1) * (1 - has_stopped)
        #     # loss2 = torch.sum(loss2) / torch.sum(1-has_stopped)
        #     #
        #     # max_mu_tensor = torch.ones_like(mu) * max_mu
        #     #
        #     # loss3 = torch.mean(nn.functional.smooth_l1_loss(mu, max_mu_tensor, reduction='none') / max_mu, 1) * (1 - has_stopped)
        #
        #
        #
        #     print('L2: %.3f' % loss2.item())
        # else:
        #     loss2 = 0
        #
        #
        #
        #
        #
        # return loss1+loss2





