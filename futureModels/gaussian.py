import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable

from torch import functional as F


no_event_margin_in_seconds = 3



def gaussian(x, mu, sig):
    return torch.exp(-torch.pow(x - mu, 2.) / (2 * sig)) * (1. / (torch.sqrt(2 * math.pi * torch.pow(sig, 2.))))


class Gaussian(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return GMMLoss(self.opt.max_prediction_horizon)

    def get_frame_indices_and_label(self, sample):
        n_frames = self.opt.sample_duration

        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
            label = seconds_before_event

        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            label = self.opt.max_prediction_horizon * 2

        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), float(label)

    def map_representation(self, outputs):
        if outputs.dim() > 1:
            mu = nn.functional.softplus(outputs[:, 0])

            tte = mu
            has_stopped = tte < (self.opt.max_prediction_horizon + .5)

        else:
            has_stopped = outputs <= self.opt.max_prediction_horizon
            tte = outputs

        has_stopped_float = has_stopped.float()
        tte = tte * has_stopped_float + 2 * self.opt.max_prediction_horizon * (1. - has_stopped_float)
        return has_stopped, tte, outputs


    def get_num_predictions(self):
        return 1

    def get_num_classes(self):
        return 2

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))


    # def init_model(self, model):
    #     for m in model.modules():
    #         if isinstance(m, nn.Linear):
    #             m.weight[:, 1].data.zero_()
    #             m.bias[1] = 2.
    #             return

    def create_pdf(self, outputs):
        mu = nn.functional.softplus(outputs[:, 0]).cpu()
        sigma = nn.functional.softplus(outputs[:, 1]).cpu()

        label = torch.arange(1, self.opt.max_prediction_horizon * 2 + 1, 1. / self.opt.eval_resolution_per_second, dtype=torch.float32)
        label = label.repeat(outputs.size(0), 1)

        mu = mu.repeat(label.size(1),1).t()
        sigma = sigma.repeat(label.size(1),1).t()


        pdf = gaussian(mu, label, sigma) + 1e-9

        output_sum = pdf.sum(dim=1)
        output_sum = output_sum.repeat(1, pdf.size(1)).view(pdf.size(0), pdf.size(1))
        output_norm = pdf / output_sum

        return output_norm



class GMMLoss(nn.Module):
    def __init__(self, max_prediction_horizon):
        super(GMMLoss, self).__init__()
        self.max_prediction_horizon = max_prediction_horizon



    def forward(self, input, target):
        assert input.size(1) == 2
        mu = nn.functional.softplus(input[:, 0])
        sigma = nn.functional.softplus(input[:, 1])


        likelihood = gaussian(mu, target, sigma) + 1e-6
        loss = -torch.log(likelihood)
        return torch.mean(loss)


        # max_mu = self.max_prediction_horizon + not_stopped_horizon
        #
        # if torch.sum(has_stopped).item() > 0:
        #     likelihood1 = torch.sum(gaussian(mu, offset, sigma), 1) + 1e-6
        #     loss1 = -torch.log(likelihood1) * has_stopped
        #     loss1 = torch.sum(loss1) / torch.sum(has_stopped)
        #     print('L1: %.3f' % loss1.item())
        # else:
        #     loss1 = 0
        #
        # if torch.sum(1 - has_stopped).item() > 0:
        #     sigma_tensor = torch.ones_like(mu) * 5.
        #
        #     likelihood2 = torch.sum(gaussian(mu, max_mu, sigma_tensor), 1) + 1e-6
        #     loss2 = -torch.log(likelihood2) * (1 - has_stopped)
        #     loss2 = torch.sum(loss2) / torch.sum(1 - has_stopped) * 0.01
        #     # loss2 = torch.mean(torch.pow(mu - max_mu, 2.)/max_mu**2, 1) * (1 - has_stopped)
        #     # loss2 = torch.sum(loss2) / torch.sum(1-has_stopped)
        #     #
        #     # max_mu_tensor = torch.ones_like(mu) * max_mu
        #     #
        #     # loss3 = torch.mean(nn.functional.smooth_l1_loss(mu, max_mu_tensor, reduction='none') / max_mu, 1) * (1 - has_stopped)
        #
        #
        #
        #     print('L2: %.3f' % loss2.item())
        # else:
        #     loss2 = 0
        #
        #
        #
        #
        #
        # return loss1+loss2





