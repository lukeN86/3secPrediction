import random
import torch
import math
import numpy as np
import os
import torch
from torch import nn
from torch.autograd import Variable
from utils import gaussian_torch

heatmap_resolution_per_second = 4
sigma = 2.
no_event_margin_in_seconds = 2
cut_off_threshold = 0.001


class HeatmapSymL2NoOffset(object):

    def __init__(self, opt):
        self.seconds_before_event = None
        self.opt = opt

    def get_training_criterion(self):
        return GMMLoss(self.opt.max_prediction_horizon)

    def get_frame_indices_and_label(self, sample):
        n_frames = 16



        if sample['label'] == 1:
            # Stopped
            start = sample['stop_frame_index'] - n_frames

            if self.seconds_before_event is None:
                max_before = int(math.floor(float(start) / self.opt.images_per_second)) - 1
                max_before = min(self.opt.max_prediction_horizon, max_before)
                seconds_before_event = random.randint(1, max_before)
            else:
                seconds_before_event = self.seconds_before_event

            start = start - seconds_before_event * self.opt.images_per_second
        else:
            # Not stopped
            rand_end = sample['n_frames'] - n_frames
            start = random.randint(1, rand_end)
            seconds_before_event = random.randint(self.opt.max_prediction_horizon + no_event_margin_in_seconds, self.opt.max_prediction_horizon * 2 + 1)


        assert start >= 1
        assert (start + n_frames) <= sample['n_frames']

        return list(range(start, start + n_frames)), float(seconds_before_event)

    def map_representation(self, outputs):
        if outputs.dim() > 1:

            hmap = outputs[:, 0, :]

            ix = torch.argmax(hmap, 1).float()
            hmap_tte = (ix / heatmap_resolution_per_second) + 1.
            stopping_threshold = self.opt.max_prediction_horizon + (no_event_margin_in_seconds / 2)
            has_stopped = hmap_tte <= stopping_threshold

            hmap = (hmap > cut_off_threshold).float() * hmap
            hmap_sum = hmap.sum(dim=1).repeat(hmap.size(1), 1).t()
            mask = hmap / hmap_sum

            pred_repeat = outputs[:, 1, :]
            masked_pred = pred_repeat * mask

            tte = masked_pred.sum(dim=1)

        else:
            has_stopped = outputs <= self.opt.max_prediction_horizon
            tte = outputs

        has_stopped_float = has_stopped.float()
        tte = tte * has_stopped_float + 2 * self.opt.max_prediction_horizon * (1. - has_stopped_float)
        return has_stopped, tte, outputs

        # ix = torch.argmax(outputs, 2).float()
        # tte = (ix / heatmap_resolution_per_second) + 1.
        # stopping_threshold = self.opt.max_prediction_horizon + (no_event_margin_in_seconds / 2)
        # has_stopped = tte <= stopping_threshold
        # has_stopped_float = has_stopped.float()
        #
        # dist = (has_stopped_float * tte) + (1 - has_stopped_float) * self.opt.max_prediction_horizon * 2
        #
        # return has_stopped, dist, outputs


    def get_num_predictions(self):
        return self.opt.max_prediction_horizon * 2 * heatmap_resolution_per_second

    def get_num_classes(self):
        return 2

    def reshape_targets(self, targets):
        return targets

    def force_seconds_before_event(self, seconds_before_event):
        assert seconds_before_event <= self.opt.max_prediction_horizon
        self.seconds_before_event = seconds_before_event
        print('Force seconds before event {}'.format(seconds_before_event))


class GMMLoss(nn.Module):
    def __init__(self, max_prediction_horizon):
        super(GMMLoss, self).__init__()
        self.max_prediction_horizon = max_prediction_horizon
        self.mse_loss = nn.MSELoss()
        self.regression_loss = nn.SmoothL1Loss(reduction='none')


    def forward(self, input, target):

        batch_size = input.size(0)

        label = torch.arange(1, self.max_prediction_horizon * 2 + 1, 1. / heatmap_resolution_per_second, dtype=torch.float32).cuda()
        label = label.repeat(batch_size, 1)
        target_repeat = target.repeat(label.size(1), 1).t().float()
        label = gaussian_torch(label, target_repeat, sigma)

        l1 = self.mse_loss(input[:, 0, :], label)

        #Regression
        label = label.detach()
        label = (label > cut_off_threshold).float() * label
        label_sum = label.sum(dim=1).repeat(label.size(1), 1).t()
        mask = label / label_sum

        reg_loss = self.regression_loss(input[:, 1, :], target_repeat)
        reg_loss = reg_loss * mask
        l2 = reg_loss.mean()


        return l1 + l2


        # mu = input[:, 0, :]
        # sigma = nn.functional.softplus(input[:, 1, :])
        #
        #
        # #Classification
        # binary_target = torch.zeros(batch_size, num_predictors, dtype=torch.int64).cuda()
        #
        # for i in range(batch_size):
        #     if target[i] <= self.max_prediction_horizon:
        #         binary_target[i, (target[i] - 1)] = 1
        #
        # cls = input[:, 2:, :]
        # logit = nn.functional.log_softmax(cls, 1)
        # binary_loss = nn.functional.nll_loss(logit, binary_target)
        #
        # #Regression
        # pred_centers = torch.arange(1, self.max_prediction_horizon + 1, self.max_prediction_horizon / num_predictors, dtype=torch.float32).cuda()
        # pred_centers = pred_centers.repeat(batch_size, 1)
        # regression_target = target.repeat(num_predictors, 1).t().float()
        #
        # offset = regression_target - pred_centers
        # likelihood = torch.sum(gaussian(mu, offset, sigma) * binary_target.float(), 1) + 1e-6
        # has_stopped = (target <= self.max_prediction_horizon).float()
        # regression_loss = -torch.log(likelihood) * has_stopped
        # regression_loss = torch.sum(regression_loss) / torch.sum(has_stopped)
        #
        # return 10. * binary_loss + regression_loss


