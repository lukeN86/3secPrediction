import time
import torch
from torch.autograd import Variable
from opts import parse_opts

from utils import get_default_image_loader, FrameIterator
from model_loader import load_model
from dataset import get_validation_data

import matplotlib
# matplotlib.use('Agg')

import matplotlib.animation as animation
# from pylab import *



import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from matplotlib.animation import FFMpegWriter
from pylab import rand
from spatial_transforms import Normalize


import numpy as np
import os

dpi = 300
orig_fps = 30

video_id = 7

#video_id = 278


seconds_before_stop = 8
seconds_after_stop = 0

def flip(x, dim):
    dim = x.dim() + dim if dim < 0 else dim
    return x[tuple(slice(None, None) if i != dim
             else torch.arange(x.size(i)-1, -1, -1).long()
             for i in range(x.dim()))]


if __name__ == '__main__':
    opt = parse_opts()

    model, time_in_future_transform = load_model(opt)

    image_loader = get_default_image_loader()
    frame_iterator = FrameIterator(opt)
    validation_data = get_validation_data(opt, frame_iterator)

    model.eval()

    _, sample, __ = validation_data[video_id]
    video_dir_path = sample['video']
    stop_ix = sample['stop_frame_index']
    if stop_ix == 0:
        stop_ix = 100



    writer = FFMpegWriter(fps=orig_fps, bitrate=4000)

    fig = plt.figure()
    gs = gridspec.GridSpec(4, 3)
    gs.update(wspace=0.5, hspace=1.0)

    ax1 = plt.subplot(gs[:, :-1])
    ax1.set_aspect('equal')
    ax1.set_xticks([])
    ax1.set_xticklabels([])
    ax1.get_yaxis().set_visible(False)

    im1 = ax1.imshow(rand(720, 1280, 3), interpolation='bilinear')
    im1.set_clim([0, 1])
    ax1.set_xlabel('original video', fontsize=8)


    ax2 = plt.subplot(gs[0, -1])


    #
    # ax2 = fig.add_subplot(224)
    # ax2.set_aspect('equal')
    # ax2.get_xaxis().set_visible(False)
    # ax2.get_yaxis().set_visible(False)
    #
    # im2 = ax2.imshow(rand(240, 427, 3), interpolation='bilinear')
    # im2.set_clim([0, 1])

    ax3 = plt.subplot(gs[1, -1])

    ax4 = plt.subplot(gs[-2:, -1])
    # im_att = ax4.imshow(rand(28, 28), interpolation='bilinear', cmap='inferno')
    # im_att.set_clim([0, 1])

    # ax5 = plt.subplot(gs[3, -1])
    # ax5.set_aspect('equal')
    # ax5.get_xaxis().set_visible(False)
    # ax5.get_yaxis().set_visible(False)
    # im_orig = ax5.imshow(rand(112, 112, 3), interpolation='bilinear')
    # im_orig.set_clim([0, 1])


    fig.set_size_inches([8, 5])

    frames = range(stop_ix - seconds_before_stop * opt.images_per_second,
          stop_ix + seconds_after_stop * opt.images_per_second)

    fps_ratio = int(orig_fps / opt.images_per_second)
    frame_ix = ((frames[0] - 1) * fps_ratio) - 2

    _, video_name = os.path.split(video_dir_path)
    _, model_name = os.path.split(opt.val_model_path)

    denormalize = Normalize(-np.array(opt.mean), [1., 1., 1.])

    with writer.saving(fig, 'D:\\3secPrediction\\videos\%s_%s.mp4' % (video_name, model_name), dpi):
        for processed_frame in frames:

            # if processed_frame == stop_ix:
            #     ax3.plot([0., 1.], [0., 1.], )

            frame_iterator.set_frame(processed_frame)
            input, _, __ = validation_data[video_id]

            if hasattr(model.module, 'keep_att_map'):
                model.module.keep_att_map = True   # Model has attention

            with torch.no_grad():
                input = input.unsqueeze(0)
                input = Variable(input)
                output = model(input)



            has_stopped, tte, prob = time_in_future_transform.map_representation(output)
            prob_len = prob.squeeze().size(0)
            prob_res = round(prob_len / (opt.max_prediction_horizon * 2))

            ax2.cla()

            prob = prob.squeeze().cpu().numpy()
            xpos = prob.argmax()
            ymax = prob[xpos]

            ax2.plot(prob, color='b')
            ax2.plot(xpos, ymax, marker='|', color='b')
            ax2.set_xlabel('$t$ [s]', fontsize=8)
            ax2.set_ylabel('$\hat{p}(t|X_N)$', fontsize=8)
            ax2.set_ylim(0, 0.1)
            ax2.set_xlim(0, (opt.max_prediction_horizon * 2) * prob_res)
            ticks = np.arange(0, opt.max_prediction_horizon + 1, 2)
            ax2.set_xticks(ticks * prob_res)
            ax2.set_xticklabels(ticks)

            if sample['label'] == 1:
                gt = (((stop_ix - processed_frame) - opt.sample_duration) / 5.)
                ax2.axvline(x=(gt + 2) * prob_res, color='r')

            ax2.axvline(x=opt.max_prediction_horizon * prob_res, color=(0.6, 0.6, 0.6), linestyle='--')

            ax3.cla()
            cumprob = prob.cumsum()
            ax3.plot(cumprob, color='b')
            ax3.set_xlabel('$t$ [s]', fontsize=8)
            ax3.set_ylabel('$\hat{F}(t|X_N)$', fontsize=8)
            ax3.set_ylim(0, 1)
            ax3.set_xlim(0, (opt.max_prediction_horizon * 2) * prob_res)
            ax3.set_xticks(ticks * prob_res)
            ax3.set_xticklabels(ticks)

            if sample['label'] == 1:
                gt = (((stop_ix - processed_frame) - opt.sample_duration) / 5.)
                ax3.axvline(x=(gt + 2) * prob_res, color='r')

            ax3.axvline(x=opt.max_prediction_horizon * prob_res, color=(0.6, 0.6, 0.6), linestyle='--')

            # t = np.arange(0., opt.max_prediction_horizon + 1, 1. / opt.eval_resolution_per_second)
            # predictions_count = opt.eval_resolution_per_second * opt.max_prediction_horizon
            # pred = np.zeros(t.shape[0])
            #
            # ax3.cla()
            # if has_stopped.item():
            #     ix = int((tte.item() * opt.eval_resolution_per_second))
            #     pred[ix:] = 1.
            #     ax3.plot(t, pred, color='g')
            # else:
            #     ax3.plot(t, pred, color='r')
            #
            # ax3.set_ylim(0, 1.)
            # ax3.set_xticks(np.arange(0, 11, step=2.))
            # ax3.set_title('predicted time to event [s]', fontsize=6)




            # small_image_path = os.path.join(video_dir_path, 'image_{:05d}.jpg'.format(processed_frame))
            # tmp = image_loader(small_image_path)
            # im2.set_data(tmp)

            real_input = denormalize(input)
            first_image = real_input[0, :, 0, :, :].int()
            # im_orig.set_data(first_image.permute(1, 2, 0))

            if hasattr(model.module, 'keep_att_map'):
                att_map = model.module.att_map

                att_sum = att_map.sum(dim=4).sum(dim=3)
                _, ix = att_sum.topk(1, 2, True)

                ix = 1
                att_map = att_map[0, 0, ix, :, :]

                # att_map = att_map.mean(dim=2).squeeze()


                att_map = att_map - att_map.mean()

                # att_map = att_map.sum(dim=2)
                # att_map = att_map[0, 0, :, :]

                att_map[:3, :] = 0
                att_map[-3:, :] = 0
                att_map[:, :3] = 0
                att_map[:, -3:] = 0

                # att_map = model.module.att_map[0, 0, :, :, :].sum(dim=0)
                # att_map = model.module.att_map[0, 0, 0, :, :]
                mx = 1. / att_map.max()
                print(mx)

                att_map = flip(att_map, 0)  # Flip because of extent coordinates in matplotlib!!
                att_map = att_map.squeeze().cpu().numpy() * 100.
                im_att = ax4.imshow(att_map, interpolation='bicubic', cmap='plasma', extent=(0, 112, 0, 112))
                im_att.set_clim([0, 1])

                ax4.imshow(first_image.permute(1, 2, 0), alpha=0.2)

                ax4.set_xticks([])
                ax4.set_xticklabels([])
                ax4.get_yaxis().set_visible(False)
                ax4.set_xlabel('network input + attention', fontsize=10)



            for i in range(0, fps_ratio):
                orig_image_path = os.path.join(video_dir_path.replace('jpg', 'jpg30fps'), 'image_{:05d}.jpg'.format(frame_ix))
                tmp = image_loader(orig_image_path)
                im1.set_data(tmp)
                writer.grab_frame()
                frame_ix = frame_ix + 1
