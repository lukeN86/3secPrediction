import torch
from torch.autograd import Variable
import time
import os
import sys
import numpy as np

from utils import AverageMeter, calculate_accuracy


def train_epoch(epoch, data_loader, model, criterion, optimizer, opt,
                epoch_logger, batch_logger, time_in_future_transform):
    print('train at epoch {}'.format(epoch))

    model.train()

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    event_accuracies = AverageMeter()
    tte_accuracies = AverageMeter()



    end_time = time.time()
    for i, (inputs, targets, velocities) in enumerate(data_loader):
        data_time.update(time.time() - end_time)

        if not opt.no_cuda:
            targets = targets.cuda(async=True)

        targets = time_in_future_transform.reshape_targets(targets)

        if targets.dtype == torch.float64:
            targets = targets.float()

        inputs = Variable(inputs)
        targets = Variable(targets)

        if hasattr(model.module, 'has_velocity'):
            if velocities.dtype == torch.float64:
                velocities = velocities.float()

            velocities = Variable(velocities)
            outputs = model(inputs, velocities)
        else:
            outputs = model(inputs)
        loss = criterion(outputs, targets)

        with torch.no_grad():
            gt_tte, _ = calculate_accuracy(outputs, targets, time_in_future_transform, opt, event_accuracies, tte_accuracies)

        losses.update(loss.data[0], inputs.size(0))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time.update(time.time() - end_time)
        end_time = time.time()

        batch_logger.log({
            'epoch': epoch,
            'batch': i + 1,
            'iter': (epoch - 1) * len(data_loader) + (i + 1),
            'loss': losses.val,
            'event_acc': event_accuracies.val,
            'tte_acc': tte_accuracies.val,
            'lr': optimizer.param_groups[0]['lr']
        })

        print('Epoch: [{0}][{1}/{2}]\t'
              'Time {batch_time.val:.3f} ({batch_time.avg:.3f}={time_per_epoch:.1f}m/ep)\t'
              'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
              'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
              'Event Acc {event_acc.val:.3f} ({event_acc.avg:.3f})\t'
              'TTE Acc {tte_acc.val:.3f} ({tte_acc.avg:.3f})\t'.format(
                  epoch,
                  i + 1,
                  len(data_loader),
                  batch_time=batch_time,
                  data_time=data_time,
                  time_per_epoch=batch_time.avg*len(data_loader)/60.,
                  loss=losses,
                  event_acc=event_accuracies,
                  tte_acc=tte_accuracies))

    epoch_logger.log({
        'epoch': epoch,
        'loss': losses.avg,
        'event_acc': event_accuracies.avg,
        'tte_acc': tte_accuracies.avg,
        'lr': optimizer.param_groups[0]['lr']
    })

    if epoch % opt.checkpoint == 0:
        save_file_path = os.path.join(opt.result_path,
                                      'save_{}.pth'.format(epoch))
        states = {
            'epoch': epoch + 1,
            'arch': opt.arch,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
        }
        torch.save(states, save_file_path)

