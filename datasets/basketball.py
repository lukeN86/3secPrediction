import torch
import torch.utils.data as data

import os
import math
import json
from tqdm import tqdm
from os import listdir
import random
import numpy as np
from utils import get_default_video_loader, print_dataset_mean
import csv

images_per_second = 5
negatives_per_video = 60



def get_class_labels(classes):
    class_labels_map = {}
    index = 0
    for class_label in classes:
        class_labels_map[class_label] = index
        index += 1
    return class_labels_map


def get_video_names_and_annotations(data, subset):
    video_names = []
    annotations = []

    for key, value in data['database'].items():
        this_subset = value['subset']
        if this_subset == subset:
            if subset == 'testing':
                video_names.append('test/{}'.format(key))
            else:
                label = value['annotations']['label']
                video_names.append('{}/{}'.format(label, key))
                annotations.append(value['annotations'])

    return video_names, annotations


def load_annotations(dataset_directory, subset_name):
    videos = {}

    annotation_file = os.path.join(dataset_directory, 'bball_dataset_april_4.csv')
    if not os.path.exists(annotation_file):
        print('Annotation file not found')
        return None

    with open(annotation_file, 'r') as csvfile:
        rowreader = csv.reader(csvfile, delimiter=',')
        for row in rowreader:
            if subset_name == 'train' and row[-1] == 'test':
                continue
            if subset_name == 'test' and row[-1] != 'test':
                continue

            anno = {
                'video_id': row[0],
                'event_start_time': row[6],
                'action_type': row[-2]
            }

            if not row[0] in videos:
                videos[row[0]] = list()

            videos[row[0]].append(anno)


    return videos




def create_positive_sample(video_path, file_count, event):

    event_start_time =  float(event['event_start_time']) / 1000

    frame_index = int(math.floor(event_start_time * images_per_second)) - 1

    if frame_index < (images_per_second * 15):
        return None

    if frame_index > (file_count - (images_per_second * 15)):
        return None

    sample = {'video': video_path, 'n_frames': file_count, 'video_id': event['video_id'],
              'label': 1, 'stop_frame_index': frame_index}

    return sample


def create_negative_samples(video_path, file_count, video_id, positive_events):

    event_padding = 30 * images_per_second

    negative_events = []

    for frame_index in range(event_padding, file_count-event_padding, round((file_count-event_padding * 2.) / negatives_per_video)):
        overlaps_positive = False

        for positive_event in positive_events:
            if abs(positive_event['stop_frame_index'] - frame_index) < event_padding:
                overlaps_positive = True
                break

        if not overlaps_positive:
            sample = {'video': video_path, 'n_frames': file_count, 'video_id': video_id,
                      'label': 0, 'event_start_time': frame_index, 'max_frame_count': event_padding}
            negative_events.append(sample)

    return negative_events




def is_event_of_interest(event):
    return 'pointer' in event['action_type']

def make_dataset(root_path, subset):

    classes = ['notStopped', 'stopped']
    class_to_idx = get_class_labels(classes)
    idx_to_class = {}
    for name, label in class_to_idx.items():
        idx_to_class[label] = name


    dataset = []
    videos = load_annotations(root_path, subset)

    for f in tqdm(videos):

        video_path = os.path.join(root_path, 'jpg', f)

        if not os.path.exists(video_path):
            print('directory %s not found' % video_path)
            continue

        file_count = len(os.listdir(video_path))
        if file_count < 100:
            print('directory %s is empty' % video_path)
            continue


        positive_events = []
        for event in videos[f]:
            if is_event_of_interest(event):
                sample = create_positive_sample(video_path, file_count, event)
                if sample is not None:
                    positive_events.append(sample)

        dataset.extend(positive_events)
        dataset.extend(create_negative_samples(video_path, file_count, f, positive_events))






    print('Total %d %s samples (%d stopped, %d not stopped)' % (len(dataset), subset,
                                                                    sum(1 for e in dataset if e['label'] == 1),
                                                                    sum(1 for e in dataset if e['label'] == 0)
                                                                    ))

    return dataset, idx_to_class


class Basketball(data.Dataset):
    """
    Args:
        root (string): Root directory path.
        spatial_transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        temporal_transform (callable, optional): A function/transform that  takes in a list of frame indices
            and returns a transformed version
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        loader (callable, optional): A function to load an video given its path and frame indices.
     Attributes:
        classes (list): List of the class names.
        class_to_idx (dict): Dict with items (class_name, class_index).
        imgs (list): List of (image path, class_index) tuples
    """

    def __init__(self,
                 root_path,
                 subset,
                 time_in_future_transform,
                 n_samples_for_each_video=1,
                 spatial_transform=None,
                 temporal_transform=None,
                 get_loader=get_default_video_loader):
        self.data, self.class_names = make_dataset(
            root_path, subset)

        self.spatial_transform = spatial_transform
        self.temporal_transform = temporal_transform
        self.loader = get_loader()
        self.time_in_future_transform = time_in_future_transform

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        sample = self.data[index]
        path = sample['video']

        frame_indices, target = self.time_in_future_transform.get_frame_indices_and_label(sample)

        if self.temporal_transform is not None:
            frame_indices = self.temporal_transform(frame_indices)
        clip = self.loader(path, frame_indices)

        if self.spatial_transform is not None:
            self.spatial_transform.randomize_parameters()
            clip = [self.spatial_transform(img) for img in clip]
        clip = torch.stack(clip, 0).permute(1, 0, 2, 3)

        return clip, target, 0

    def __len__(self):
        return len(self.data)
