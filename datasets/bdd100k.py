import torch
import torch.utils.data as data

import os
import math
import functools
import json
from tqdm import tqdm
from os import listdir
import random
import numpy as np
from utils import get_default_video_loader, print_dataset_mean


images_per_second = 5


def get_class_labels(classes):
    class_labels_map = {}
    index = 0
    for class_label in classes:
        class_labels_map[class_label] = index
        index += 1
    return class_labels_map


def get_video_names_and_annotations(data, subset):
    video_names = []
    annotations = []

    for key, value in data['database'].items():
        this_subset = value['subset']
        if this_subset == subset:
            if subset == 'testing':
                video_names.append('test/{}'.format(key))
            else:
                label = value['annotations']['label']
                video_names.append('{}/{}'.format(label, key))
                annotations.append(value['annotations'])

    return video_names, annotations


def load_annotations_from_dir(dataset_directory, subset_name):
    loc = {}
    info_path = os.path.join(dataset_directory, 'info', '100k', subset_name)
    files = listdir(info_path)

    if os.environ.get('FAST_DATA_LOAD') is not None:
        files = files[1:1000]

    for f in tqdm(files):
        with open(os.path.join(info_path, f)) as fp:
            try:
                data = json.load(fp)
                loc[f] = data['locations']
            except:
                print('Cannot process file %s' % f)

    return loc


def find_stoppings(locations):
    MIN_STOP_DURATION = 5
    SPEED_THRESHOLD = 2.0

    prev_speed = float('nan')
    stop_counter = 0
    stop_location = None
    has_stopped = False

    for location in locations:
        speed = location['speed']
        if not math.isnan(prev_speed) and prev_speed > SPEED_THRESHOLD and speed < SPEED_THRESHOLD:
            stop_location = location  # Vehicle has stopped
            stop_counter = 0
            has_stopped = True
        elif stop_location is not None and speed < SPEED_THRESHOLD:
            stop_counter += 1
            if stop_counter >= MIN_STOP_DURATION:
                return stop_location, has_stopped
        else:
            stop_location = None
            stop_counter = 0

        prev_speed = speed

    return None, has_stopped


def create_sample(root_path, subset, file_name, locations, stop_location):

    video_name, ext = os.path.splitext(file_name)
    video_path = os.path.join(root_path, 'jpg', '100k', subset, video_name)

    if not os.path.exists(video_path):
        print('directory %s not found' % video_path)
        return None

    file_count = len(os.listdir(video_path))
    if file_count < 100:
        print('directory %s is empty' % video_path)
        return None

    first_timestamp = locations[0]['timestamp']

    if stop_location[0] is not None:
        # Vehicle has stopped
        start = stop_location[0]['timestamp']
        stop_frame_index = int((start - first_timestamp) / 1000 * images_per_second)
        if stop_frame_index < (14 * images_per_second) or stop_frame_index > file_count:
            return None     # Not enough data before/after the event

        class_ix = 1  # Stopped
    elif not stop_location[1]:
        # Vehicle has never stopped, take 'random' sample from the video
        class_ix = 0  # Not stopped
        stop_frame_index = 0
    else:
        # Vehicle has stopped but only for a short period of time -> ignore
        return None

    velocities = list()
    for frame_ix in range(file_count):
        offset = round(frame_ix / images_per_second * 1000)
        frame_timestamp = first_timestamp + offset

        velocity = 0

        for i in range(len(locations)):
            if locations[i]['timestamp'] > frame_timestamp:
                if i > 0:
                    velocity = (locations[i]['speed'] + locations[i-1]['speed']) / 2
                else:
                    velocity = locations[i]['speed']

                break

        velocities.append(velocity)

    assert len(velocities) == file_count

    sample = {'video': video_path, 'n_frames': file_count, 'video_id': video_name,
              'label': class_ix, 'stop_frame_index': stop_frame_index, 'velocities': velocities}

    return sample




def make_dataset(root_path, subset):

    classes = ['notStopped', 'stopped']
    class_to_idx = get_class_labels(classes)
    idx_to_class = {}
    for name, label in class_to_idx.items():
        idx_to_class[label] = name

    cache_file = os.path.join(root_path, 'bdd100k_%s_cache.npy' % subset)
    if os.path.exists(cache_file):
        print('Loading dataset cache %s' % cache_file)
        dataset = np.load(cache_file)
    else:
        dataset = []
        locations = load_annotations_from_dir(root_path, subset)

        for f in tqdm(locations):

            if len(locations[f]) == 0:
                continue

            stp = find_stoppings(locations[f])
            sample = create_sample(root_path, subset, f, locations[f], stp)

            if sample is not None:
                dataset.append(sample)

        np.save(cache_file, dataset)

    # print_dataset_mean(dataset)

    # if subset == 'val':
    #     for sample in dataset:
    #         with open(os.path.join('D:\\BDD100k\\export', '%s.json' % sample['video_id']), 'w') as opt_file:
    #             json.dump(sample, opt_file)


    print('Total %d %s samples (%d stopped, %d not stopped)' % (len(dataset), subset,
                                                                    sum(1 for e in dataset if e['label'] == 1),
                                                                    sum(1 for e in dataset if e['label'] == 0)
                                                                    ))

    return dataset, idx_to_class


class BDD100k(data.Dataset):
    """
    Args:
        root (string): Root directory path.
        spatial_transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        temporal_transform (callable, optional): A function/transform that  takes in a list of frame indices
            and returns a transformed version
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        loader (callable, optional): A function to load an video given its path and frame indices.
     Attributes:
        classes (list): List of the class names.
        class_to_idx (dict): Dict with items (class_name, class_index).
        imgs (list): List of (image path, class_index) tuples
    """

    def __init__(self,
                 root_path,
                 subset,
                 time_in_future_transform,
                 n_samples_for_each_video=1,
                 spatial_transform=None,
                 temporal_transform=None,
                 get_loader=get_default_video_loader):
        self.data, self.class_names = make_dataset(
            root_path, subset)

        self.spatial_transform = spatial_transform
        self.temporal_transform = temporal_transform
        self.loader = get_loader()
        self.time_in_future_transform = time_in_future_transform

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        sample = self.data[index]
        path = sample['video']

        frame_indices, target = self.time_in_future_transform.get_frame_indices_and_label(sample)

        if self.temporal_transform is not None:
            frame_indices = self.temporal_transform(frame_indices)
        clip = self.loader(path, frame_indices)

        if self.spatial_transform is not None:
            self.spatial_transform.randomize_parameters()
            clip = [self.spatial_transform(img) for img in clip]
        clip = torch.stack(clip, 0).permute(1, 0, 2, 3)

        velocity = list()
        for frame_ix in frame_indices:
            velocity.append(float(sample['velocities'][frame_ix-1]))

        return clip, target, np.array(velocity)

    def __len__(self):
        return len(self.data)
