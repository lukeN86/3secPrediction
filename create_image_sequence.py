import time
import torch
from torch.autograd import Variable
from opts import parse_opts

from utils import get_default_image_loader, FrameIterator
from model_loader import load_model
from dataset import get_validation_data

import matplotlib
# matplotlib.use('Agg')

import matplotlib.animation as animation
# from pylab import *



import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from matplotlib.animation import FFMpegWriter
from pylab import rand

from spatial_transforms import Compose, CenterCrop, Scale, Normalize
from utils import first_nonzero

import numpy as np
import os
from torch import nn
import torchvision as tv

from PIL import Image

dpi = 300
orig_fps = 30


video_id = 74

# D:\3secPrediction\results_20181025_resnet-34_heatmapsym

seconds_before_stop = 3
seconds_after_stop = 0

dir_path = 'D:\\3secPrediction\\videos'
dir_path = 'D:\\Basketball'

def flip(x, dim):
    dim = x.dim() + dim if dim < 0 else dim
    return x[tuple(slice(None, None) if i != dim
             else torch.arange(x.size(i)-1, -1, -1).long()
             for i in range(x.dim()))]

if __name__ == '__main__':
    opt = parse_opts()
    opt.video_path = dir_path

    model, time_in_future_transform = load_model(opt)

    image_loader = get_default_image_loader()
    frame_iterator = FrameIterator(opt)
    validation_data = get_validation_data(opt, frame_iterator)

    model.eval()

    _, sample, __ = validation_data[video_id]
    video_dir_path = sample['video']
    # stop_ix = sample['stop_frame_index']
    #
    # if stop_ix == 0:
    stop_ix = 50

    spatial_transform = Compose([
        Scale(opt.sample_size),
        CenterCrop(opt.sample_size)
    ])


    frames = range(stop_ix - seconds_before_stop * opt.images_per_second,
          stop_ix + seconds_after_stop * opt.images_per_second)

    fps_ratio = int(orig_fps / opt.images_per_second)
    frame_ix = ((frames[0] - 1) * fps_ratio) + 1

    _, video_name = os.path.split(video_dir_path)
    _, model_name = os.path.split(opt.val_model_path)

    out_path = os.path.join(dir_path, video_name)

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    denormalize = Normalize(-np.array(opt.mean), [1., 1., 1.])

    for processed_frame in frames:


        frame_iterator.set_frame(processed_frame)
        input, _, __ = validation_data[video_id]

        if hasattr(model.module, 'keep_att_map'):
            model.module.keep_att_map = True   # Model has attention

        with torch.no_grad():
            input = input.unsqueeze(0)
            input = Variable(input)
            output = model(input)

        has_stopped, tte, prob = time_in_future_transform.map_representation(output)

        prob_len = prob.squeeze().size(0)
        prob_res = round(prob_len / (opt.max_prediction_horizon * 2))
        frame_string = 'vid_{:05d}_frame_{:05d}'.format(video_id, processed_frame)

        plt.figure(figsize=(3, 2))
        plt.rc('font', family='serif')

        prob = prob.squeeze().cpu().numpy()
        xpos = prob.argmax()
        ymax = prob[xpos]

        plt.plot(prob, color='b')
        plt.plot(xpos, ymax, marker='|', color='b')
        plt.xlabel('$t$ [s]', fontsize=10)
        plt.ylabel('$\hat{p}(t|X_N)$', fontsize=10)
        plt.ylim(0, 0.1)
        plt.xlim(0, (opt.max_prediction_horizon * 2) * prob_res)
        ticks = np.arange(0, opt.max_prediction_horizon + 1, 2)
        plt.xticks(ticks*prob_res, ticks)

        if sample['label'] == 1:
            gt = (((stop_ix - processed_frame) - opt.sample_duration) / 5.)
            plt.axvline(x=(gt + 1) * prob_res, color='r')

        plt.axvline(x=opt.max_prediction_horizon * prob_res, color=(0.6, 0.6, 0.6), linestyle='--')

        plt.tight_layout()
        plt.savefig(os.path.join(out_path, frame_string + '_prob.pdf'))
        plt.savefig(os.path.join(out_path, frame_string + '_prob.eps'))
        plt.close()

        plt.figure(figsize=(3, 2))
        plt.rc('font', family='serif')

        cumprob = prob.cumsum()
        plt.plot(cumprob, color='b')
        plt.xlabel('$t$ [s]', fontsize=10)
        plt.ylabel('$\hat{F}(t|X_N)$', fontsize=10)
        plt.ylim(0, 1)
        plt.xlim(0, (opt.max_prediction_horizon * 2) * prob_res)
        ticks = np.arange(0, opt.max_prediction_horizon + 1, 2)
        plt.xticks(ticks * prob_res, ticks)

        if sample['label'] == 1:
            gt = (((stop_ix - processed_frame) - opt.sample_duration) / 5.)
            plt.axvline(x=gt * prob_res, color='r')

        plt.axvline(x=opt.max_prediction_horizon * prob_res, color=(0.6, 0.6, 0.6), linestyle='--')

        plt.tight_layout()
        plt.savefig(os.path.join(out_path, frame_string + '_cdf.pdf'))
        plt.savefig(os.path.join(out_path, frame_string + '_cdf.eps'))
        plt.close()

        plt.figure(figsize=(3, 3))
        if hasattr(model.module, 'keep_att_map'):
            att_map = model.module.att_map

            # att_sum = att_map.sum(dim=4).sum(dim=3)
            # _, ix = att_sum.topk(1, 2, True)

            ix = 0
            att_map = att_map[0, 0,ix,  :, :]

            att_map = att_map - att_map.mean()

            # att_map = att_map.sum(dim=2)
            # att_map = att_map[0, 0, :, :]


            att_map[:3,:] = 0
            att_map[-3:, :] = 0
            att_map[:, :3] = 0
            att_map[:, -3:] = 0




            # att_map = model.module.att_map[0, 0, :, :, :].sum(dim=0)
            # att_map = model.module.att_map[0, 0, 0, :, :]
            mx = 1. / att_map.max()
            print(mx)

            att_map = flip(att_map, 0) # Flip because of extent coordinates in matplotlib!!
            att_map = att_map.squeeze().cpu().numpy() * 1000.
            ax = plt.imshow(att_map, interpolation='bicubic', cmap='plasma', extent=(0, 112, 0, 112))
            ax.set_clim([0, 1])




        real_input = denormalize(input)
        first_image = real_input[0, :, 0, :, :].int()
        plt.imshow(first_image.permute(1, 2, 0), alpha=0.2)

        plt.axis('off')
        plt.tight_layout()


        plt.savefig(os.path.join(out_path, frame_string + '_att.jpg'))
        plt.close()

        orig_image_path = os.path.join(video_dir_path.replace('jpg', 'jpg30fps'), 'image_{:05d}.jpg'.format(round((processed_frame) * (orig_fps / 5))+12))

        orig_image_path = os.path.join(video_dir_path,
                                        'image_{:05d}.jpg'.format(processed_frame))
        im = image_loader(orig_image_path)

        w, h = im.size
        th, tw = [h, h]
        x1 = int(round((w - tw) / 2.))
        y1 = int(round((h - th) / 2.))
        im = im.crop((x1, y1, x1 + tw, y1 + th))

        im.save(os.path.join(out_path, frame_string + '_orig.jpg'))





        # small_image_path = os.path.join(video_dir_path, 'image_{:05d}.jpg'.format(processed_frame))
        # tmp = image_loader(small_image_path)
        # im2.set_data(tmp)

        # im2.set_data(input[:, 0, :, :].permute(1, 2, 0))







