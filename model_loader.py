import torch
import json
import os

from model import generate_model
from mean import get_mean, get_std
from future_prediction_model import get_future_prediction_model


def find_last_model(model_directory):
    files = [f for f in os.listdir(model_directory) if f.endswith('.pth')]
    files.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
    model_path = os.path.join(model_directory, files[-1])
    return model_path


def load_model_state_for_validation(model, opt):
    model_path = find_last_model(opt.val_model_path)

    print('loading model {}'.format(model_path))
    checkpoint = torch.load(model_path)
    assert opt.arch == checkpoint['arch']
    model.load_state_dict(checkpoint['state_dict'])


def load_model(opt):


    opts_file = os.path.join(opt.val_model_path, 'opts.json')
    if not os.path.exists(opts_file):
        print('File %s not found' % opts_file)
        exit(-1)

    with open(opts_file, 'r') as opt_file:
        loaded_opt = json.load(opt_file)
        opt.model = loaded_opt['model']
        opt.model_depth = loaded_opt['model_depth']
        opt.future_prediction = loaded_opt['future_prediction']
        opt.dataset = loaded_opt['dataset']
        opt.mean = loaded_opt['mean']
        opt.std = loaded_opt['std']
        opt.sample_size = loaded_opt['sample_size']
        opt.sample_duration = loaded_opt['sample_duration']
        opt.max_prediction_horizon = loaded_opt['max_prediction_horizon']
        opt.heatmap_resolution_per_second = loaded_opt['heatmap_resolution_per_second']

    opt, time_in_future_transform, _ = get_future_prediction_model(opt)
    opt.n_val_samples = 1
    opt.pretrain_path = ''
    opt.arch = '{}-{}'.format(opt.model, opt.model_depth)

    model, _ = generate_model(opt)

    load_model_state_for_validation(model, opt)

    return model, time_in_future_transform