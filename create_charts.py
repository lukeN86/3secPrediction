import os
import sys
import matplotlib
import numpy as np
matplotlib.use('Agg')

import matplotlib.pyplot as plt

if __name__=="__main__":
    dir_path = sys.argv[1]
    dataset = sys.argv[2]

    models_all = [
        ['results_20181013_timeInFutureRegression', 'L1 regression'],
        ['results_20181025_resnet-34_timeRangeToEvent', 'Binary Classifiers'],
        ['results_20181025_resnet_softatt2-34_timeRangeToEvent', 'Binary Classifiers + Soft Attention per frame'],
        ['results_20181025_resnet_stn2-34_timeRangeToEvent', 'Binary Classifiers + Hard Attention'],
        ['results_20181025_resnet-34_heatmap', 'Heatmap'],
        ['results_20181025_resnet_stn2-34_heatmap', 'Heatmap + Hard Attention'],
        # ['results_20181025_resnet-34_heatmap2', 'Heatmap beyond horizon'],
        ['results_20181025_resnet-34_gmm9', 'GMM'],
        ['results_20181025_resnet-34_gmm11', 'GMMv11'],
        ['results_20181004_timeRangeOneInMany', 'Binary classifier (one in many)'],
        ['results_20181025_resnet-34_heatmappadded', 'Heatmap (padded)'],
        ['results_20181025_resnet_softatt3-34_timeRangeToEvent', 'Binary Classifiers + Soft Attention global'],
        ['results_20181025_resnet-34_heatmapsym', 'Heatmap (symmetric)'],
        ['bdd100k_20181107_resnet-34_heatmapsymextendboundaries', 'Heatmap (ext boundaries)'],
        ['results_20181025_resnet-34_heatmapsyml2', 'Heatmap + L2'],
        ['results_20181025_resnet-34_heatmapsyml1', 'Heatmap + L1'],
        # ['results_20181025_resnet-50_heatmapsym', 'Heatmap (Resnet50)'],



    ]


    if dataset == 'basketball':
        models_paper = [
            ['basketball_8frames_5sec_resnet-34_timeRangeOneInMany', 'One-in-many Classifier', ''],
            ['basketball_8frames_5sec_224_resnet-34_timeRangeToEvent', 'Binary Classifiers'],
            ['basketball_8frames_5sec_224_resnet-34_timeInFutureRegression', 'Direct Regression'],
            ['', 'Gaussian Distribution'],
            ['', 'Weibull Distribution'],
            ['basketball_8frames_5sec_224_resnet-34_heatmapsym', 'Heuristic Heatmap'],
            ['basketball_8frames_5sec_224_resnet-34_heatmapgmm', 'Gaussian Mixture', 'b'],
        ]
        max_prediction_horizon = 5
    elif dataset == 'bdd100k':
        models_paper = [
            ['results_20181004_timeRangeOneInMany', 'One-in-many Classifier'],
            ['results_20181025_resnet-34_timeRangeToEvent', 'Binary Classifiers'],
            ['results_20181013_timeInFutureRegression', 'Direct Regression', ''],
            ['bdd100k_20181107_8frames_resnet-34_gaussian', 'Gaussian Distribution', ''],
            ['results_20181013_pretrain_weibull', 'Weibull Distribution', ''],
            ['results_20181025_resnet-34_heatmapsym', 'Heuristic Heatmap'],
            ['results_20181025_resnet-34_heatmapgmm', 'Gaussian Mixture', 'b'],
        ]
        max_prediction_horizon = 10
    elif dataset == 'human':
        models_paper = [
            ['results_20181025_resnet-34_heatmapgmm', 'Our model (Gaussian Mixture)', 'b'],
            ['human', 'Non-expert Human', 'r']
        ]
        max_prediction_horizon = 10



    models = models_paper


    #plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    # Event accuracy
    plt.figure(figsize=(4, 3))
    plt.xlabel('time before event $\Delta$ [s]', fontsize=10)
    plt.ylabel('Event Accuracy [%]', fontsize=10)
    plt.ylim(0.65, .85)

    x_label = np.arange(1, max_prediction_horizon+1)
    for i in range(len(models)):
        model = models[i]
        if len(model[0]) > 0:

            if len(model) >= 3 and model[2] == '':
                continue

            accuracy = np.load(os.path.join(dir_path, '{}_{}_eval.npy'.format(model[0], dataset)))

            line, = plt.plot(x_label, accuracy[:, 0], label=model[1], linewidth=2.0)
            if i > 9:
                line.set_dashes([3,3])

            if len(model) >= 3:
                line.set_color(model[2])

    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(dir_path, 'eventAccuracy_%s.pdf' % dataset))
    plt.savefig(os.path.join(dir_path, 'eventAccuracy_%s.eps' % dataset))
    plt.close()

    # Time to Event

    plt.figure(figsize=(4, 3))
    plt.xlabel('time before event $\Delta$ [s]', fontsize=10)
    plt.ylabel('Time to Event Error [s]', fontsize=10)
    plt.ylim(0, 4.)

    x_label = np.arange(1,  max_prediction_horizon+1)
    for i in range(len(models)):
        model = models[i]
        if len(model[0]) > 0:

            if len(model) >= 3 and model[2] == '':
                continue

            accuracy = np.load(os.path.join(dir_path, '{}_{}_eval.npy'.format(model[0], dataset)))
            line, = plt.plot(x_label, accuracy[:, 1], label=model[1], linewidth=2.0)
            if i > 9:
                line.set_dashes([3,3])

            if len(model) >= 3:
                line.set_color(model[2])

    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(dir_path, 'timeToEvent_%s.pdf' % dataset))
    plt.savefig(os.path.join(dir_path, 'timeToEvent_%s.eps' % dataset))
    plt.close()

    # Table

    file = open(os.path.join(dir_path, 'eval_%s.tex' % dataset), 'w')

    # file.write('model & EA & TTEE & CDFE \\\\\n')

    for model in models:
        if len(model[0]) > 0:
            accuracy = np.load(os.path.join(dir_path, '{}_{}_eval.npy'.format(model[0], dataset)))
            avg_accuracy = accuracy[:, 0].sum() / accuracy.shape[0]
            avg_tte = accuracy[:, 1].sum() / accuracy.shape[0]
            if accuracy.shape[1] == 3:
                avg_cdfe = accuracy[:, 2].sum() / accuracy.shape[0]
            else:
                avg_cdfe = 0
        else:
            avg_accuracy = 0.
            avg_tte = 0.
            avg_tte = 0.
            avg_cdfe = 0.

        file.write(('%s & %.2f & %.2f & %.2f ' % (model[1], avg_accuracy * 100., avg_tte, avg_cdfe)) + '\\\\\n')

    file.close()
